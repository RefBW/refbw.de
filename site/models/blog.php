<?php

class BlogPage extends Page
{
    /**
     * Generates byline with page details
     *
     * @return string
     */
    public function byline(): string
    {
        return 'Blog rund um Referendariat & Zweites Staatsexamen';
    }


    /**
     * Collects most popular tags among all blog posts
     *
     * @param int $count Number of tags
     * @return array Most popular tags
     */
    public function popularTags(int $count = 5): array
    {
        # Retrieve blog posts
        $posts = collection('posts');

        # Fetch tags being used
        $tags = $posts->pluck('tags', ',', true);

        # Loop over each tag ..
        $tags = A::sort(array_map(function(string $tag) use($posts) {
            # .. counting its occurence
            return [
                'name' => $tag,
                'count' => $posts->filterBy('tags', $tag, ',')->count(),
            ];
        }, $tags), 'count');

        return array_slice(A::pluck($tags, 'name'), 0, $count);
    }


    /**
     * Fetches items for tabs of main navigation
     *
     * @param int $count Number of items
     * @return array
     */
    public function tabbedNav(int $count): array
    {
        # Create data array
        $data = [
            [
                'title' => 'Alle Beiträge',
                'url' => $this->url(),
            ]
        ];

        foreach ($this->popularTags($count) as $tag) {
            $data[] = [
                'title' => $tag,
                'url' => url($this->id(), ['params' => ['thema' => $tag]]),
            ];
        }

        return $data;
    }
}

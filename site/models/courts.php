<?php

class CourtsPage extends Page
{
    /**
     * Generates byline with page details
     *
     * @return string
     */
    public function byline(): string
    {
        return 'Alle Stammdienststellen im Überblick';
    }


    /**
     * Fetches items for tabs of main navigation
     *
     * @return array
     */
    public function tabbedNav(): array
    {
        # Create data array
        $data = [
            [
                'title' => $this->heading(),
                'url' => $this->url(),
            ],
        ];

        foreach (['Karlsruhe', 'Stuttgart'] as $olg) {
            $data[] = [
                'title' => sprintf('OLG-Bezirk %s', $olg),
                'url' => sprintf('%s#%s', $this->url(), Str::lower($olg)),
            ];
        }

        return $data;
    }
}

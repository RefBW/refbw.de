<?php

use Kirby\Cms\Page;


class JobsUploadPage extends Page
{
    /**
     * Fetches page to be redirected to after successful form submission
     *
     * @return Kirby\Cms\Page
     */
    public function redirectPage(): Page
    {
        # If proper sibling page exists ..
        if ($redirect = $this->siblings()->find('danke')) {
            # .. use it as redirect target
            return $redirect;
        }

        # .. otherwise use homepage
        return site()->homePage();
    }
}

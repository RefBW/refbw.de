<?php

class BlogPostPage extends Page
{
    /**
     * Fetches byline about author
     *
     * @return string
     */
    public function by(): string
    {
        # Get author
        $user = $this->author()->toUser();

        if ($user->isAdmin()) {
            return 'vom digitalen Hausmeister';
        }

        if ($user->isLeader()) {
            return 'von den Sprechervorständen';
        }

        return sprintf('vom Team des LG %s)', $user->court()->toCourt());
    }


    /**
     * Fetches post excerpt
     *
     * @return string
     */
    public function getExcerpt(): string
    {
        # Fetch excerpt length
        $length = $this->length()->int();

        return $this->summary($length > 0 ? $length : 286);
    }
}

<?php

class JobsPage extends Page
{
    /**
     * Generates byline with page details
     *
     * @return string
     */
    public function byline(): string
    {
        return 'Stationsausbildung, Nebenverdienst & Berufseinstieg';
    }


    /**
     * Collects employment types among all job offers
     *
     * @param int $count Number of tags
     * @return array Employment types
     */
    public function employmentTypes(int $count = 5): array
    {
        # Retrieve job offers
        $jobs = collection('jobs/active');

        # Fetch tags being used
        $tags = $jobs->pluck('period', ',', true);

        # Loop over each tag ..
        $tags = A::sort(array_map(function(string $tag) use ($jobs) {
            # .. counting its occurence
            return [
                'name' => $tag,
                'count' => $jobs->filterBy('period', $tag, ',')->count(),
            ];
        }, $tags), 'count');

        return array_slice(A::pluck($tags, 'name'), 0, $count);
    }


    /**
     * Fetches items for tabs of main navigation
     *
     * @param int $count Number of items
     * @return array
     */
    public function tabbedNav(int $count): array
    {
        # Create data array
        $data = [
            [
                'title' => 'Alle Angebote',
                'url' => $this->url(),
            ]
        ];

        foreach ($this->employmentTypes($count) as $tag) {
            $data[] = [
                'title' => $tag,
                'url' => url($this->id(), ['params' => ['abschnitt' => $tag]]),
            ];
        }

        return $data;
    }


    /**
     * Creates report for 'stats' section
     *
     * @return array
     */
    public function reports(): array
    {
        # Store panel link to 'jobs' page
        $dbLink = '/pages/stellenangebote+anbieter';

        # Build base report
        # (1) Fetch offerers & jobs
        $offerers = $this->offerers()->toStructure();
        $jobs = kirby()->collection('jobs/all');

        # (2) Put everything together
        $total = [
            'value' => sprintf('%s Anzeigen (%s Anbieter)', $jobs->count(), $offerers->count()),
            'label' => 'in der Datenbank gespeichert',
            'theme' => 'info',
            'link' => $dbLink,
        ];

        # Build monthly report
        # (1) Retrieve jobs for current month
        $date = date('Y-m');
        $count = $jobs->filter(function($job) use($date) {
            return $job->date()->toDate('Y-m') === $date;
        })->count();

        # (2) Calculate increase (in percent)
        $increase = $count / $jobs->count() * 100;

        # (3) Put everything together
        $month = [
            'value' => sprintf('%s Anzeigen', $count),
            'label' => 'diesen Monat hinzugefügt',
            'info' => sprintf('%s %s ', $count > 0 ? '+' : '±', round($increase, 2)) . '%',
            'theme' => $count > 0 ? 'positive' : 'info',
            'link' => $dbLink,
        ];

        return [$total, $month];
    }
}

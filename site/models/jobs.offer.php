<?php

class JobsOfferPage extends Page
{
    /**
     * Fetches offerer
     *
     * @return null|Kirby\Cms\StructureObject
     */
    public function getOfferer()
    {
        # Fetch offerer
        return $this->offerer()->toPage();
    }


    /**
     * Fetches homepage
     *
     * @return string
     */
    public function getHomepage(): string
    {
        # Fetch offerer
        return $this->homepage()->or($this->getOfferer()->homepage());
    }


    /**
     * Fetches contact email
     *
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email()->or($this->getOfferer()->email());
    }


    /**
     * Fetches job excerpt
     *
     * @return string|null
     */
    public function getExcerpt(): ?string
    {
        # Fetch offerer
        $offerer = $this->getOfferer();

        if ($excerpt = $this->text()->or($offerer->text())) {
            return $excerpt;
        }

        # Fetch 'text' blocks
        $blocks = $this->blocks()->toBlocks()->filterBy('type', 'text');

        # If at least one is available ..
        if ($blocks->count() > 0) {
            # .. use it
            return $blocks->first();
        }

        return null;
    }
}

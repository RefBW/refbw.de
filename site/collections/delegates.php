<?php

use Kirby\Cms\App;


/**
 * Fetches Higher Regional Court speakers
 *
 * @param Kirby\Cms\App $kirby
 * @return Kirby\Cms\Users
 */
return function (App $kirby)
{
    return $kirby->users()->filterBy('role', 'leader')->sortBy('name');
};

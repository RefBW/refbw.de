<?php

use Kirby\Cms\App;
use Kirby\Cms\Page;
use Kirby\Cms\Pages;


/**
 * Fetches Regional Courts
 *
 * @param Kirby\Cms\App $kirby
 * @return Kirby\Cms\Pages
 */
return function (App $kirby): Pages
{
    # Get all jobs
    $jobs = $kirby->collection('jobs/all')->filter(function (Page $job) {
        return $job->date()->toDate() >= $job->ends()->toDate();
    });

    # Fetch sticky ones and ..
    $sticky = $jobs->filterBy(function(Page $page) {
        return $page->sticky()->toBool();
    })->flip();

    # .. put them first
    return $sticky->add($jobs->not($sticky)->flip());
};

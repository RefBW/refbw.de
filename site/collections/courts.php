<?php

use Kirby\Cms\Site;
use Kirby\Cms\Pages;


/**
 * Fetches Regional Courts
 *
 * @param Kirby\Cms\Site $site
 * @return Kirby\Cms\Pages
 */
return function (Site $site): Pages
{
    return $site->find('landgerichte')
                ->children()
                ->listed()
                ->sortBy('title');
};

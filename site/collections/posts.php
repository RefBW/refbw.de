<?php

use Kirby\Cms\Site;
use Kirby\Cms\Pages;


/**
 * Fetches blog posts
 *
 * @param Kirby\Cms\Site $site
 * @return Kirby\Cms\Pages
 */
return function (Site $site): Pages
{
    # Get all jobs
    $all = $site->find('aktuelles')
        ->children()
        ->filterBy('template', 'blog.post');

    # Fetch sticky ones and ..
    $sticky = $all->filterBy(function($page) {
        return $page->sticky()->toBool();
    })->flip();

    # .. put them first
    return $sticky->add($all->not($sticky)->flip());
};

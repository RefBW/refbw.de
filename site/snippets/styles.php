<style lang="windi">
    /**
     * Fonts
     */
    @font-face {
        font-display: swap;
        font-family: 'Bitter';
        font-style: normal;
        font-weight: 400;
        src:
            url('/assets/fonts/Bitter-Regular.woff2') format('woff2'),
            url('/assets/fonts/Bitter-Regular.woff') format('woff'),
            url('/assets/fonts/Bitter-Regular.ttf') format('truetype'),
            url('/assets/fonts/Bitter-Regular.eot') format('embedded-opentype');
    }

    @font-face {
        font-display: swap;
        font-family: 'Bitter';
        font-style: normal;
        font-weight: 500;
        src:
            url('/assets/fonts/Bitter-Medium.woff2') format('woff2'),
            url('/assets/fonts/Bitter-Medium.woff') format('woff'),
            url('/assets/fonts/Bitter-Medium.ttf') format('truetype'),
            url('/assets/fonts/Bitter-Medium.eot') format('embedded-opentype');
    }

    @font-face {
        font-display: swap;
        font-family: 'Bitter';
        font-style: normal;
        font-weight: 600;
        src:
            url('/assets/fonts/Bitter-SemiBold.woff2') format('woff2'),
            url('/assets/fonts/Bitter-SemiBold.woff') format('woff'),
            url('/assets/fonts/Bitter-SemiBold.ttf') format('truetype')
            url('/assets/fonts/Bitter-SemiBold.eot') format('embedded-opentype');
    }


    /**
     * Typography
     */

    ::selection {
        @apply bg-primary-800;
        @apply text-white;
    }

    h1,
    h2,
    h3,
    h4,
    h5,
    h6 {
        @apply font-serif;
    }

    .bookmark {
        @apply mb-4 px-4 py-2;
        @apply inline-block;
        @apply relative -left-8;
        @apply text-2xl text-white;
        @apply bg-primary-600;
        @apply rounded-r;
    }

    .heading {
        @apply mb-1;
        @apply font-semibold;
        @apply text-xl text-primary-600;
    }

    .list {
        @apply ml-12;
        @apply list-disc list-outside;
        list-style-image: url("data:image/svg+xml;charset=utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 10 11' width='10' height='11'%3E%3Cpath d='M6 4V0H4v4H0v2h4v4h2V6h4V4H6z' fill='%23059669'/%3E%3C/svg%3E");
    }

    .link {
        @apply font-medium text-secondary-500;
        @apply hover:underline;
    }

    .media .link {
        @apply text-primary-600;
    }


    /**
     * Margins
     */

    .text + .text,
    .text + .list,
    .list + .list,
    .list + .text {
        @apply mt-2;
    }

    .text + .media,
    .list + .media,
    .media + .text,
    .media + .list {
        @apply mt-4;
    }

    .text + .heading,
    .list + .heading,
    .media + .media,
    .media + .heading,
    .heading + .media {
        @apply mt-6;
    }

    .text + .bookmark,
    .list + .bookmark,
    .media + .bookmark {
        @apply mt-8;
    }


    /**
     * Banner
     */

    @screen lg {
        .hero-gradient {
            background: linear-gradient(0deg, rgba(255, 255, 255, 1), rgba(255, 255, 255, 0))
        }
    }


    /**
     * Sidebar
     */

    .sidebar-item + .sidebar-item {
        @apply mt-6;
    }

    .sidebar-heading {
        @apply font-semibold;
        @apply text-lg;
    }

    .link-sm + .link-sm {
        @apply mt-2
    }

    .link-lg + .link-lg {
        @apply mt-3;
    }


    /**
     * Visibility
     */

    [x-cloak] {
        @apply hidden;
    }


    /**
     * Forms
     */

    .form-label {
        @apply mb-2;
        @apply block;
        @apply font-serif font-medium;
    }

    .form-input {
        @apply px-3 py-2;
        @apply w-full;
        @apply border-2 border-dashed border-primary-600 focus:border-secondary-500;
        @apply placeholder-primary-700 placeholder-opacity-100;
        @apply focus:outline-none;
        @apply rounded;
        @apply resize-none;
    }

    .form-button {
        @apply w-full;
        @apply py-3;
        @apply flex justify-center items-center;
        @apply font-serif text-2xl text-white;
        @apply bg-primary-600 hover:bg-secondary-500 focus:bg-secondary-500 transition;
        @apply cursor-pointer rounded;
        @apply focus:outline-none focus:ring-4 focus:ring-secondary-500 focus:ring-opacity-50;
    }

    .form-required {
        @apply font-semibold;
        @apply text-secondary-500;
    }

    .form-error .form-input {
        @apply border-red-400;
        @apply placeholder-red-400 placeholder-opacity-100;
    }

    .form-error span {
        @apply text-red-400;
    }

    .uniform__potty {
        @apply absolute;
        left: -9999px;
    }


    /**
     * Embeds
     */

    .embed {
        @apply relative;
        padding-bottom: 56.25%;
    }

    .embed iframe {
        @apply w-full h-full;
        @apply absolute;
        @apply inset-0;
    }

    .embed-svg {
        @apply w-32 h-32;
        @apply transition;
    }

    .embed-svg.is-off {
        @apply block;
        @apply group-hover:hidden;
        @apply text-primary-600;
        @apply opacity-100 group-hover:opacity-0;
    }

    .embed-svg.is-on {
        @apply hidden;
        @apply group-hover:block;
        @apply text-secondary-500;
        @apply opacity-0 group-hover:opacity-100;
    }


    /**
     * Cards
     */

    .card {
        @apply font-serif;
        @apply border-2 border-dashed border-primary-600 hover:border-secondary-500;
        @apply text-black hover:text-white;
        @apply bg-white hover:bg-secondary-500;
        @apply transition;
        @apply rounded;
    }

    .card.is-active {
        @apply border-secondary-500;
        @apply text-secondary-500;
    }

    .basicLightbox{position:fixed;display:flex;justify-content:center;align-items:center;top:0;left:0;width:100%;height:100vh;background:rgba(0,0,0,.8);opacity:.01;transition:opacity .4s ease;z-index:1000;will-change:opacity}.basicLightbox--visible{opacity:1}.basicLightbox__placeholder{max-width:100%;transform:scale(.9);transition:transform .4s ease;z-index:1;will-change:transform}.basicLightbox__placeholder>iframe:first-child:last-child,.basicLightbox__placeholder>img:first-child:last-child,.basicLightbox__placeholder>video:first-child:last-child{display:block;position:absolute;top:0;right:0;bottom:0;left:0;margin:auto;max-width:95%;max-height:95%}.basicLightbox__placeholder>iframe:first-child:last-child,.basicLightbox__placeholder>video:first-child:last-child{pointer-events:auto}.basicLightbox__placeholder>img:first-child:last-child,.basicLightbox__placeholder>video:first-child:last-child{width:auto;height:auto}.basicLightbox--iframe .basicLightbox__placeholder,.basicLightbox--img .basicLightbox__placeholder,.basicLightbox--video .basicLightbox__placeholder{width:100%;height:100%;pointer-events:none}.basicLightbox--visible .basicLightbox__placeholder{transform:scale(1)}
</style>

<div class="posts flex flex-col space-y-4">
    <?php foreach ($data as $item) : ?>
    <a class="" href="<?= $item->url() ?>">
        <article class="card group px-6 py-4 pt-4 py-6 <?php e($item->sticky()->toBool(), 'relative') ?>">
            <header class="mb-1 leading-none">
                <time class="font-medium text-sm" datetime="<?= $item->date()->toDate('Y-m-d') ?>"><?= $item->date()->toDate('d.m.Y') ?></time>
                <?php if ($item->intendedTemplate() == 'jobs.offer') : ?>
                <span class="font-medium text-sm">- <?= $item->getOfferer()->title() ?></span>
                <?php endif ?>
                <h3 class="heading text-2xl group-hover:text-white transition"><?= $item->title() ?></h3>
            </header>
            <?php if ($excerpt = $item->getExcerpt()) : ?>
            <p class="text-base"><?= $excerpt ?></p>
            <?php endif ?>
            <?php if ($item->sticky()->toBool()) : ?>
            <div class="absolute top-2 right-2 text-secondary-500 group-hover:text-white transition" title="Favorit" role="img">
                <?= useSymbol('star-filled', 'w-6 h-6') ?>
            </div>
            <?php endif ?>
        </article>
    </a>
    <?php endforeach ?>
</div>

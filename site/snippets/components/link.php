<?php

# Configuration
#
# - link
# - size
#
# - url
# - title
# - icon

$url = $url ?? $link->url();
$title = $title ?? $link->name();
$icon = $icon ?? 'link';

snippet('components/link/' . $size, compact('url', 'title', 'icon'));

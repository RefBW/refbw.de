<?php

# Configuration
#
# - download
# - size
#
# - icon

$icon = $icon ?? 'download-filled';

snippet('components/download/' . $size, compact('download', 'icon'));

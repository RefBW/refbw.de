<?php
    # Configuration
    #
    # - url
    # - title
    # - padding
    # - icon
    # - iconClass
    # - fontClass

    if ($url) :

    # Check whether link is external
    if (Str::contains($url, 'mailto:')) {
        $isExternal = true;
    }

    else {
        $isExternal = isExternal($url);
    }
?>
<a
    class="card group <?= $padding ?> flex items-center"
    href="<?= esc($url) ?>"
    <?php e($isExternal, 'target="_blank"') ?>
>
    <div class="flex-none flex items-center text-primary-600 group-hover:text-white transition">
        <?= useSymbol($icon, $iconClass) ?>
    </div>
    <div class="flex-1 flex items-center text-primary-600 group-hover:text-white transition">
        <span class="<?= $fontClass ?>"><?= $title ?></span>
    </div>
</a>
<?php endif ?>

<?php

# Configuration
#
# - url
# - title

$padding = 'link-lg p-3';
$iconClass = 'w-5 h-5';
$fontClass = 'ml-2 font-semibold text-lg';

snippet('components/link/base', compact('url', 'title', 'padding', 'icon', 'iconClass', 'fontClass'));

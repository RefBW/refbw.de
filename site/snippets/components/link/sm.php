<?php

# Configuration
#
# - url
# - title

$padding = 'link-sm p-2';
$iconClass = 'w-4 h-4';
$fontClass = 'ml-1 font-medium text-base';

snippet('components/link/base', compact('url', 'title', 'padding', 'icon', 'iconClass', 'fontClass'));

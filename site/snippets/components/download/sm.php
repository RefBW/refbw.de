<?php

# Configuration
#
# - download

$padding = 'p-2';
$linkClass = 'link-sm';
$iconClass = 'w-4 h-4';
$fontClass = 'ml-1 font-medium text-base';

snippet('components/download/base', compact('download',  'padding', 'linkClass', 'iconClass', 'fontClass'));

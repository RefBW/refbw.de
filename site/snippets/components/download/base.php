<?php
    # Configuration
    #
    # - download
    # - padding
    # - linkClass
    # - iconClass
    # - fontClass

    if ($file = $download->file()->toFile()) :
?>
<a
    class="card group <?= $linkClass ?> <?= $padding ?> flex items-center relative"
    href="<?= esc($file->url()) ?>"
    download="<?= Str::slug($download->name())?>"
>
    <div class="flex items-center text-primary-600 group-hover:text-white transition">
        <div class="flex-none flex items-center">
            <?= useSymbol('download-filled', $iconClass) ?>
        </div>
        <div class="flex-1 <?= $fontClass ?>">
            <span class=""><?= $download->name() ?></span>
        </div>
    </div>
    <div class="absolute inset-0 <?= $padding ?> flex items-center text-white bg-secondary-500 opacity-0 group-hover:opacity-100 transition rounded">
        <?= useSymbol('download-filled', $iconClass) ?>
        <span class="<?= $fontClass ?>"><?= Str::upper($file->extension()) ?> (<?= $file->niceSize() ?>)</span>
    </div>
</a>
<?php endif ?>

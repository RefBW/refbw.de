<?php

# Configuration
#
# - download

$padding = 'p-3';
$linkClass = 'link-lg';
$iconClass = 'w-5 h-5';
$fontClass = 'ml-2 font-semibold text-lg';

snippet('components/download/base', compact('download',  'padding', 'linkClass', 'iconClass', 'fontClass'));

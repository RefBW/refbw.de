<?php

# Configuration
#
# - size
# - filter
# - tagName
#
# - uid
# - icon

use Kirby\Toolkit\A;


# Fetch query parameters
$params = params();

# Determine whether current query is active
$isActive = false;

if ($tag = param($tagName)) {
    $isActive = $tag == $filter;
}

# If current query is active ..
if ($isActive) {
    # .. remove it from parameters
    $params = A::without($params, $tagName);

# .. otherwise ..
} else {

    # .. add it to parameters
    $params = A::update($params, [$tagName => $filter]);
}

# Build filter URL
$url = url($uid ?? $page->id(), ['params' => $params]);

# Determine icon class
$icon = $icon ?? 'tag';

if ($isActive) {
    $icon .= '-filled';
}

snippet('components/filter/' . $size, compact('url', 'filter', 'isActive', 'icon'));

<?php

# Configuration
#
# - url
# - filter
# - isActive

$padding = 'link-lg p-3';
$iconClass = 'w-5 h-5';
$fontClass = 'ml-2 font-semibold text-lg';

snippet('components/filter/base', compact('url', 'filter', 'isActive', 'padding', 'icon', 'iconClass', 'fontClass'));

<?php

# Configuration
#
# - url
# - filter
# - isActive

$padding = 'link-sm p-2';
$iconClass = 'w-4 h-4';
$fontClass = 'ml-1 font-medium text-base';

snippet('components/filter/base', compact('url', 'filter', 'isActive', 'padding', 'icon', 'iconClass', 'fontClass'));

<?php
    # Configuration
    #
    # - url
    # - isActive
    # - filter
    # - padding
    # - icon
    # - iconClass
    # - fontClass

    if ($url) :
?>
<a
    class="card <?php e($isActive, 'is-active') ?> group <?= $padding ?> flex items-center"
    href="<?= esc($url) ?>"
>
    <div class="flex-none flex items-center <?php e($isActive, 'text-secondary-500', 'text-primary-600') ?> group-hover:text-white transition">
        <?= useSymbol($icon, $iconClass) ?>
    </div>
    <div class="flex-1 flex items-center <?php e($isActive, 'text-secondary-500', 'text-primary-600') ?> group-hover:text-white transition">
        <span class="<?= $fontClass ?>"><?= $filter ?></span>
    </div>
</a>
<?php endif ?>

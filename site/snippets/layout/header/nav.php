<!-- TAB NAVIGATION -->
<div
    class="hidden lg:block"
    x-data="{ tab: '<?= $page->indexPage()->uid() ?>' }"
>
    <!-- BUTTONS -->
    <?php
        # Invoke main navigation
        $mainNav = $site->mainNav();
    ?>
    <div class="px-4 lg:px-8 pb-2 lg:pb-4 flex <?php e($mainNav->count() > 4, 'justify-between', 'justify-around') ?> items-end text-primary-900">
        <?php foreach ($mainNav as $parent) : ?>
        <a
            class="flex flex-col font-semibold <?php e($parent->isOpen() || $page->isChildOf($parent), 'text-secondary-500') ?> hover:text-secondary-500 focus:text-secondary-500"
            href="<?= $parent->aliasPage()->url() ?>"
            title="<?= $parent->title() ?>"
            @mouseover="tab = '<?= $parent->uid() ?>'"
        >
            <div class="flex justify-center">
                <?= $parent->symbol('w-6 h-6') ?>
            </div>
            <span class="text-xl"><?= $parent->title() ?></span>
        </a>
        <?php endforeach ?>
    </div>

    <!-- TABS -->
    <div class="h-12 px-4 bg-primary-700">
        <?php foreach ($mainNav as $parent) : ?>
        <div class="flex justify-start" x-show="tab == '<?= $parent->uid() ?>'" x-cloak>
            <?php foreach ($parent->tabbedNav(3) as $item) : ?>
            <?php if ($page->isActiveTab($item['url'])) : ?>
            <span class="px-8 flex-none h-12 flex justify-center items-center font-medium text-white bg-primary-600 underline cursor-default">
                <?= $item['title'] ?>
            </span>
            <?php else : ?>
            <a
                class="px-8 flex-none h-12 flex justify-center items-center font-medium text-white hover:bg-primary-600"
                href="<?= $item['url'] ?>"
            >
                <?= $item['title'] ?>
            </a>
            <?php endif ?>
            <?php endforeach ?>
        </div>
        <?php endforeach ?>
    </div>
</div>

<!-- TOP NAVIGATION -->
<div
    class="h-full text-white rounded-t"
    x-data="menu"
>
    <!-- TOP BAR -->
    <div class="px-4 py-2 h-12 flex items-center bg-primary-900 z-10 rounded-t">
        <div class="flex-1">
            <a
                class="text-sm hover:underline"
                href="<?= $site->url() ?>"
            >
                <?= $site->title() ?>
            </a>
        </div>
        <div class="flex-none">
            <div class="hidden lg:flex lg:space-x-4">
                <?php foreach ($site->topNav() as $child) : ?>
                <a
                    class="flex items-center hover:underline"
                    href="<?= $child->url() ?>"
                >
                    <?= $child->symbol('w-5 h-5') ?>
                    <span class="ml-2 text-sm"><?= $child->title() ?></span>
                </a>
                <?php endforeach ?>
                <a
                    class="flex items-center hover:underline"
                    href="<?= Panel::url() ?>"
                >
                    <?= useSymbol('panel', 'w-5 h-5') ?>
                    <span class="ml-2 text-sm">Login</span>
                </a>
            </div>
            <!-- HAMBURGER -->
            <button
                class="flex justify-center lg:hidden"
                @click="isOpen = !isOpen"
                type="button"
            >
                <span
                    class="flex justify-center text-white hover:text-secondary-500 focus:text-white transition"
                    x-show="!isOpen"
                >
                    <?= useSymbol('menu', 'w-6 h-6') ?>
                </span>
                <template x-if="isOpen" x-cloak>
                    <span class="flex justify-center text-white hover:text-secondary-500 focus:text-white transition">
                        <?= useSymbol('x', 'w-6 h-6') ?>
                    </span>
                </template>
            </button>
        </div>
    </div>

    <!--COLLAPSABLE MENU-->
    <div
        class="mt-12 h-60 justify-center items-center absolute top-0 left-0 right-0 z-10 bg-primary-700 shadow-inner"
        :class="isOpen ? 'flex' : 'hidden'"
        @click.outside="isOpen = false"
        x-show="isOpen"
        x-cloak
    >
        <div class="flex flex-col space-y-2">
            <?php foreach ($site->mainNav() as $child) : ?>
            <a
                class="mr-6 flex items-center font-medium hover:text-secondary-500 focus:text-secondary-500"
                href="<?= $child->url() ?>"
            >
                <?= $child->symbol('w-6 h-6') ?>
                <span class="ml-2 text-xl"><?= $child->title() ?></span>
            </a>
            <?php endforeach ?>
        </div>
    </div>
</div>

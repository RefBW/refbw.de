<?php
    # Title
    $title = $page->isHomePage() ? $site->title() : sprintf('%s | %s', $page->title(), $site->title());

    # Description
    $description = esc($page->summary());

    # Metaimage
    $image = $page->cover();
?>

<!-- Schema -->
<style itemscope itemtype="https://schema.org/WebSite" itemref="schema_name schema_description schema_image"></style>

<!-- Title -->
<title><?= $title ?></title>
<meta id="schema_name" itemprop="name" content="<?= $title ?>">

<!-- Description -->
<meta name="description" content="<?= $description ?>">
<meta id="schema_description" itemprop="description" content="<?= $description ?>">

<!-- Canonical URL -->
<link rel="canonical" href="<?= $page->url() ?>">

<!-- Last modified -->
<meta name="date" content="<?= $page->modified('Y-m-d') ?>" scheme="YYYY-MM-DD">

<!-- Feed -->
<link rel="alternate" type="application/rss+xml" title="<?= page('aktuelles')->title() ?>" href="<?= url('feed') ?>"/>

<!-- Metaimage -->
<meta id="schema_image" itemprop="image" content="<?= $image->focusPreset('meta')->url() ?>">

<!-- Open Graph -->
<meta property="og:type" content="website">
<meta property="og:title" content="<?= $title ?>">
<meta property="og:description" content="<?= $description ?>">
<?php if ($openGraph = $image->toOpenGraph()) : ?>
<meta property="og:image" content="<?= $openGraph->url() ?>">
<meta property="og:width" content="<?= $openGraph->width() ?>">
<meta property="og:height" content="<?= $openGraph->height() ?>">
<?php endif ?>
<meta property="og:site_name" content="<?= $site->title() ?>">
<meta property="og:url" content="<?= $page->url() ?>">
<meta property="og:locale" content="de_DE">

<!-- Twitter Card -->
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:title" content="<?= $title ?>">
<meta name="twitter:description" content="<?= $description ?>">
<?php if ($twitterCard = $image->toTwitterCard()) : ?>
<meta name="twitter:image" content="<?= $twitterCard->url() ?>">
<?php endif ?>

<footer class="flex-none" role="contentinfo">
    <!-- NAVIGATION -->
    <?php snippet('layout/footer/nav') ?>

    <!-- BOTTOM BAR -->
    <?php snippet('layout/footer/bar') ?>
</footer>

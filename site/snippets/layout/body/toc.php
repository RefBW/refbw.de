<?php if ($toc = $page->toc()) : ?>
<div
    class="flex flex-col absolute -top-5 right-0 text-white bg-secondary-500 shadow rounded-l z-10"
    x-data="{ isOpen: false }"
>
    <button
        class="px-3 py-2 flex justify-end items-center hover:bg-secondary-400 transition"
        :class="isOpen ? 'rounded-tl' : 'rounded-l'"
        @click="isOpen = !isOpen"
        @click.outside="isOpen = false"
    >
        <div class="flex items-center" x-show="!isOpen">
            <?= useSymbol('light-bulb', 'w-5 h-5') ?>
        </div>
        <template x-if="isOpen" x-cloak>
            <?= useSymbol('light-bulb-filled', 'w-5 h-5') ?>
        </template>
        <h4 class="ml-2 font-medium text-base">Inhaltsübersicht</h4>
    </button>
    <div
        class="py-1 rounded-l"
        x-show="isOpen"
        x-cloak
    >
        <?php foreach ($toc as $heading) : ?>
        <?php if ($heading['level'] == 'h2') : ?>
        <a class="block py-1 px-3 w-full text-right text-sm hover:text-secondary-700 transition" href="#<?= Str::slug($heading['text']) ?>">
            <?= $heading['text'] ?>
        </a>
        <?php else : ?>
        <a class="block py-1 px-3 w-full text-right text-xs hover:text-secondary-700 transition" href="#<?= Str::slug($heading['text']) ?>">
            <?= $heading['text'] ?>
        </a>
        <?php endif ?>
        <?php endforeach ?>
    </div>
</div>
<?php endif ?>

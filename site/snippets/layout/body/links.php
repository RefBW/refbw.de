<?php
    # Determine page type
    $isHomeOrError = $page->isHomeOrErrorPage();

    # Set defaults
    $pages = $page->siblings(false)->listed();
    $heading = $page->indexPage()->title();

    # If 'linkable' children are available ..
    if ($page->hasLinkableChildren()) {
        # .. fetch them instead
        $pages =  $page->linkableChildren();
        $heading = $page->title();
    }

    # Same goes for home page ..
    if ($isHomeOrError) {
        # .. but with main pages
        $pages =  $site->children->listed();
        $heading = $page->title();
    }

    if ($pages->isNotEmpty()) :
?>
<?php if (!$isHomeOrError) : ?>
<h2 class="bookmark"><?= $heading ?> im Überblick</h2>
<?php endif ?>
<div class="media">
    <div class="grid grid-cols-none md:grid-cols-2 gap-4">
        <?php foreach ($pages as $child) : ?>
        <a
            class="card group px-6 py-4 flex flex-col justify-center"
            href="<?= $child->url() ?>"
        >
            <div class="flex-none flex items-center text-primary-600 group-hover:text-white transition">
                <?= $child->symbol('w-8 h-8') ?>
                <h3 class="ml-4 my-1 font-semibold text-xl"><?= $child->title() ?></h3>
            </div>
            <div class="flex-1">
                <p class="pl-12 text-base"><?= $child->byline() ?></p>
            </div>
        </a>
        <?php endforeach ?>
    </div>
</div>
<?php endif ?>

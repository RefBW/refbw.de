<nav class="px-4 py-6 min-h-32 flex flex-col sm:flex-row items-center justify-between font-serif text-2xl text-white bg-primary-600">
    <div class="flex items-center">
        <?php if ($nav['hasLeft']) : ?>
        <a
            href="<?= $nav['leftURL'] ?>"
            class="flex items-center hover:underline"
        >
            <?= $site->symbol('chevron-left', 'w-8 h-8') ?>
            <span class="ml-6"><?= $nav['leftTitle'] ?></span>
        </a>
        <?php else : ?>
        <div class="flex items-center text-primary-900 cursor-default">
            <?= $site->symbol('chevron-left', 'w-8 h-8') ?>
            <?php if (isset($nav['rightEmpty'])) : ?>
            <span class="ml-6"><?= $nav['leftEmpty'] ?></span>
            <?php endif ?>
        </div>
        <?php endif ?>
    </div>

    <div class="flex items-center">
        <?php if ($nav['hasRight']) : ?>
        <a
            href="<?= $nav['rightURL'] ?>"
            class="flex items-center hover:underline"
        >
            <span class="mr-6"><?= $nav['rightTitle'] ?></span>
            <?= $site->symbol('chevron-right', 'w-8 h-8') ?>
        </a>
        <?php else : ?>
        <div class="flex items-center text-primary-900 cursor-default">
            <?php if (isset($nav['rightEmpty'])) : ?>
            <span class="mr-6"><?= $nav['rightEmpty'] ?></span>
            <?php endif ?>
            <?= $site->symbol('chevron-right', 'w-8 h-8') ?>
        </div>
        <?php endif ?>
    </div>
</nav>

<nav
    class="mb-8 flex items-center font-serif font-medium text-sm text-white"
    role="navigation" aria-label="breadcrumb"
>
    <?php foreach($site->breadcrumb()->slice() as $crumb) : ?>
    <?php if ($crumb->isActive()) : ?>
    <span class="select-none"><?= $crumb->title() ?></span>
    <?php else : ?>
    <a
        class="text-white hover:text-primary-900"
        href="<?= $crumb->url() ?>"
    >
        <?php e($crumb->isHomePage(), 'Start', $crumb->title()) ?>
    </a>
    <?php endif ?>
    <?php if (!$crumb->isActive()) : ?>
    <span class="mx-2 select-none">»</span>
    <?php endif ?>
    <?php endforeach ?>
</nav>

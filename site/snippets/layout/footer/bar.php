<div class="px-4 py-3 flex flex-col lg:flex-row justify-between items-center font-serif text-white bg-primary-900 rounded-b">
    <div class="mb-2 lg:mb-0 text-center">
        <span class="text-sm text-center">
            &copy; 2021 - <?= date('Y') ?> &middot; <?= $site->title() ?>
        </span>
    </div>
    <div class="mt-4 lg:mt-0 flex-none justify-end grid grid-cols-2 sm:grid-cols-3 gap-4 md:gap-0 md:flex md:space-x-4">
        <a class="flex items-center hover:underline" href="https://xn--baw-joa.social/@RefBW" title="Folgt uns ins Fediverse" target="_blank" rel="me">
            <?= useSymbol('mastodon', 'w-5 h-5') ?>
            <span class="ml-2 text-sm">Mastodon</span>
        </a>
        <a class="flex items-center hover:underline" href="https://codeberg.org/refbw/refbw.de" title="Quellcode auf Codeberg" target="_blank">
            <?= useSymbol('codeberg', 'w-5 h-5') ?>
            <span class="ml-2 text-sm">Quellcode</span>
        </a>
        <a class="flex items-center hover:underline" href="<?= site()->url() ?>/feed" title="Abonniert unsere Neuigkeiten">
            <?= useSymbol('rss-filled', 'w-5 h-5') ?>
            <span class="ml-2 text-sm">RSS</span>
        </a>
        <?php foreach ($site->bottomNav() as $child) : ?>
        <a class="flex items-center hover:underline" href="<?= $child->url() ?>" title="Unser <?= $child->title() ?>">
            <?= $child->symbol('w-5 h-5') ?>
            <span class="ml-2 text-sm"><?= $child->title() ?></span>
        </a>
        <?php endforeach ?>
    </div>
</div>

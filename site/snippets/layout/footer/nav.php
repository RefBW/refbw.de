<div class="px-8 pt-12 pb-8 grid grid-cols-2 sm:grid-cols-3 lg:grid-cols-6 gap-6 font-serif text-white bg-primary-700">
    <?php foreach ($site->children()->listed() as $parent) : ?>
    <div class="">
        <h6 class="mb-2 inline-block font-medium text-base border-b-2 border-primary-600 hover:border-secondary-500">
            <?= Html::a($parent->url(), $parent->title()) ?>
        </h6>
        <ul class="text-sm leading-relaxed">
            <?php foreach ($parent->linkableChildren() as $child) : ?>
            <li><?= Html::a($child->url(), $child->title(), ['class' => 'hover:underline']) ?></li>
            <?php endforeach ?>
        </ul>
    </div>
    <?php endforeach ?>
</div>

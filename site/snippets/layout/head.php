<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- FONTS -->
    <link rel="preload" href="<?= url('assets/fonts/Bitter-Regular.woff2') ?>" as="font" type="font/woff2" crossorigin>
    <link rel="preload" href="<?= url('assets/fonts/Bitter-Medium.woff2') ?>" as="font" type="font/woff2" crossorigin>

    <!-- ASSETS -->
    <?php
        echo Bnomei\Fingerprint::css('assets/css/main.css', ['integrity' => true]);
        echo Bnomei\Fingerprint::js('assets/js/main.js', ['integrity' => true, 'defer' => true]);
    ?>

    <!-- METADATA -->
    <?php snippet('layout/head/metadata') ?>

    <!-- ANALYTICS -->
    <script data-goatcounter="https://stats.refbw.de/count" async src="//stats.refbw.de/count.js"></script>

    <!-- FAVICONS -->
    <?php snippet('layout/head/favicons') ?>
</head>

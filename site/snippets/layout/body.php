<main id="content" class="flex-1 bg-white" role="main">
    <!-- PAGE HEADER -->
    <?php slot('header') ?>
    <header class="px-8 pt-6 pb-8 text-white bg-primary-600">
        <?php snippet('layout/body/breadcrumbs') ?>

        <h1 class="text-4xl"><?php e($page->template() == 'court', 'Landgericht ') ?><?= $page->heading()->or($page->title()) ?></h1>
        <p class="mt-4 font-serif text-2xl text-primary-900">
            <?= $page->text()->kti() ?>
        </p>
    </header>
    <?php endslot() ?>

    <!-- PAGE CONTENT -->
    <section class="px-8 py-12 relative">
        <?php if ($page->hasToc()) snippet('layout/body/toc') ?>

        <?php slot('page-header') ?>
        <?php endslot() ?>

        <?php if ($sidebar = $page->snippet('sidebar')) : ?>
        <div class="mb-8 flex flex-col lg:flex-row">
            <div class="lg:w-5/7">
                <?php slot() ?>
                <?= $page->blocks()->toBlocks() ?>
                <?php endslot() ?>
            </div>
            <div class="mt-8 lg:mt-0 lg:pl-12 lg:w-2/7">
                <?= $sidebar ?>
            </div>
        </div>
        <?php else : ?>
        <?php slot() ?>
        <?= $page->blocks()->toBlocks() ?>
        <?php endslot() ?>
        <?php endif ?>

        <?php slot('page-footer') ?>
        <?php endslot() ?>

        <?php if ($page->hasPageLinks()) snippet('layout/body/links') ?>
    </section>

    <!-- PAGE FOOTER -->
    <?php if (isset($nav)) snippet('layout/body/pagination', compact('nav')) ?>
</main>

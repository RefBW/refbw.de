<header id="site-header" class="flex-none font-serif bg-white rounded-t" role="banner">
    <!-- TOP BAR -->
    <?php snippet('layout/header/bar') ?>

    <!-- HERO IMAGE -->
    <?php if ($hero = $site->hero()->focusPreset('hero')) : ?>
    <div class="relative">
        <?php if ($file = $hero->original()) : ?>
        <img
            src="<?= $hero->url() ?>"
            width="<?= $hero->width() ?>"
            height="<?= $hero->height() ?>"
            title="<?= $file->titleAttribute() ?>"
            alt="<?= $file->altAttribute() ?>"
            loading="eager"
        >
        <?php endif ?>
        <div class="hero-gradient absolute inset-0 flex flex-col justify-end">
            <?php snippet('layout/header/nav') ?>
        </div>
    </div>
    <?php endif ?>
</header>

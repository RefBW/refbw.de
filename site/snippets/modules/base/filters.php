<?php
    # Configuration
    #
    # - heading
    # - isOpen
    # - filters
    # - size

    $heading = $heading ?? 'Filter';
    $isOpen = $isOpen ?? true;
    $tagName = $tagName ?? Str::slug($heading);
    $size = $size ?? 'sm';

    # If query parameter is active ..
    if ($tag = param($tagName)) {
        # .. always expand its menu
        $isOpen = true;
    }

    if (!empty($filters)) :
?>
<div
    class="sidebar-item"
    x-data="{ isOpen: <?php e($isOpen, 'true', 'false') ?> }"
>
    <button
        class="w-full flex justify-between items-center text-primary-600 hover:text-secondary-500 transition"
        @click="isOpen = !isOpen"
    >
        <?php if (!empty($heading)) : ?>
        <h5 class="sidebar-heading"><?= $heading ?></h5>
        <?php endif ?>
        <div class="flex items-center" x-show="!isOpen">
            <?= useSymbol('plus', 'w-6 h-6') ?>
        </div>
        <template x-if="isOpen" x-cloak>
            <?= useSymbol('minus', 'w-6 h-6') ?>
        </template>
    </button>
    <div class="mt-2" x-show="isOpen" x-cloak>
        <?php foreach ($filters as $filter) snippet('components/filter', compact('filter', 'tagName', 'size')) ?>
    </div>
</div>
<?php endif ?>

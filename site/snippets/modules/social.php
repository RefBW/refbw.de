<?php
    # Configuration
    #
    # - heading
    # - social
    # - size

    $heading = $heading ?? 'Für Referendar:innen vor Ort';
    $size = $size ?? 'sm';

    if ($social->isNotEmpty()) :
?>
<div class="sidebar-item">
    <h5 class="sidebar-heading text-primary-600"><?= $heading ?></h5>
    <div class="mt-2">
        <?php foreach ($social as $link) snippet('components/link', ['link' => $link, 'title' => Str::ucfirst($link->name()), 'icon' => $link->name(), 'size' => $size]) ?>
    </div>
</div>
<?php endif ?>

<?php
    # Configuration
    #
    # - heading
    # - email
    # - homepage
    # - social
    # - size

    $heading = $heading ?? 'Kontaktmöglichkeiten';
    $size = $size ?? 'sm';

    $hasHomepage = !empty($homepage);
    $hasEmail = !empty($email);

    if ($social->isNotEmpty() || $hasHomepage || $hasEmail) :
?>
<div class="sidebar-item">
    <h5 class="sidebar-heading text-primary-600"><?= $heading ?></h5>
    <div class="mt-2">
        <?php if ($hasEmail) : ?>
        <?php snippet('components/link', ['url' => 'mailto:' . $email, 'title' => 'Email schreiben', 'icon' => 'mail', 'size' => $size]) ?>
        <?php endif ?>
        <?php if ($hasHomepage) : ?>
        <?php snippet('components/link', ['url' => $homepage, 'title' => 'Homepage', 'icon' => 'home', 'size' => $size]) ?>
        <?php endif ?>
        <?php foreach ($social as $link) snippet('components/link', ['link' => $link, 'title' => Str::ucfirst($link->name()), 'icon' => $link->name(), 'size' => $size]) ?>
    </div>
</div>
<?php endif ?>

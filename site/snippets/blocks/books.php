<?php

/**
 * @var \Kirby\Cms\Block $block
 */

if ($books = $block->rows()->toStructure()) :

?>
<?php foreach($books as $book) : ?>
<div
    class="media flex flex-col lg:flex-row"
    x-data="lightbox"
>
    <div class="flex-1 flex items-center">
        <div>
            <header class="mb-1 leading-none">
                <span class="font-medium text-sm"><?= $book->autor() ?></span>
                <h3 class="font-semibold text-lg text-primary-600"><?= $book->name() ?></h3>
            </header>
            <?= $book->text()->kt() ?>
            <footer class="mt-2 flex justify-end font-medium text-base italic">
                Preis: <?= $book->price()->toEuro() ?> Euro &middot; ISBN: <?= $book->isbn() ?>
            </footer>
        </div>
    </div>
    <?php
        if ($cover = $book->cover()->toFile()) :
        $thumb = $cover->thumb('book');
    ?>
    <figure class="mt-6 lg:mt-0 ml-12 flex-none flex justify-center items-center">
        <div
            class="relative rounded cursor-pointer"
            @click="show()"
        >
            <img
                data-lazyload
                class="js-lightbox rounded"
                data-src="<?= $thumb->url() ?>"
                width="<?= $thumb->width() ?>"
                height="<?= $thumb->height() ?>"
                data-lightbox="<?= $cover->url() ?>"
                data-width="<?= $cover->width() ?>"
                data-height="<?= $cover->height() ?>"
                alt="<?= $cover->altAttribute() ?>"
            >
            <figcaption class="absolute inset-0 px-16 flex flex-col justify-center items-center font-serif text-2xl text-white text-center bg-secondary-500 opacity-0 hover:opacity-90 transition rounded">
                <?= useSymbol('book-open', 'w-16 h-16 mb-4') ?>
                Ansicht vergrößern
            </figcaption>
        </div>
    </figure>
    <?php endif ?>
</div>
<?php endforeach ?>
<?php endif ?>

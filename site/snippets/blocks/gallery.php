<?php

/**
 * @var \Kirby\Cms\Block $block
 */

?>
<figure
    class="media"
    x-data="masonry"
>
    <ul class="js-masonry">
        <?php
            foreach ($block->images()->toFiles() as $image) :
            $thumb = $image->thumb('gallery');
            $lightbox = $image->thumb('lightbox');
        ?>
        <li
            class="overflow-hidden"
            x-data="lightbox"
        >
            <div
                class="flex flex-col bg-secondary-500 relative rounded cursor-pointer"
                @click="show()"
            >
                <img
                    data-lazyload
                    class="js-lightbox w-full rounded"
                    data-src="<?= $thumb->url() ?>"
                    width="<?= $thumb->width() ?>"
                    height="<?= $thumb->height() ?>"
                    alt="<?= $image->altAttribute() ?>"
                    data-lightbox="<?= $lightbox->url() ?>"
                    data-width="<?= $lightbox->width() ?>"
                    data-height="<?= $lightbox->height() ?>"
                    style="object-fit: cover; object-position: <?= $image->focusPercentageX() ?>% <?= $image->focusPercentageY() ?>%;"
                    <?php e($image->source()->isNotEmpty(), sprintf('title="Quelle: %s"', esc($image->source()))) ?>
                >
                <?php if ($image->titleAttribute()->isNotEmpty()) : ?>
                <div class="absolute inset-0 px-6 flex flex-col justify-center items-center font-serif text-base text-white text-center bg-secondary-500 opacity-0 hover:opacity-90 transition rounded">
                    <?= useSymbol('photograph', 'w-12 h-12 mb-4') ?>
                    <?= $image->titleAttribute() ?>
                </div>
                <?php endif ?>
            </div>
        </li>
        <?php endforeach ?>
    </ul>
</figure>

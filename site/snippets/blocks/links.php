<?php

/**
 * @var \Kirby\Cms\Block $block
 */

$links = $block->rows()->toStructure();

if ($links->isNotEmpty()) :

?>
<div class="media mt-4">
    <?php foreach($links as $link) : ?>
    <?php if ($url = $link->url()) : ?>
    <a
        class="card group <?php e(!$link->isFirst(), 'mt-2') ?> px-4 py-2 flex flex-col justify-center"
        href="<?= esc($url) ?>"
        <?php e(isExternal($url), 'target="_blank"') ?>
    >
        <div class="flex-none flex items-center text-primary-600 group-hover:text-white transition">
            <?= useSymbol('link-filled', 'w-5 h-5') ?>
            <span class="ml-1 my-1 font-medium text-lg"><?= $link->name() ?></span>
        </div>
    </a>
    <?php endif ?>
    <?php endforeach ?>
</div>
<?php endif ?>

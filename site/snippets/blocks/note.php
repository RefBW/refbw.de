<?php

/**
 * @var \Kirby\Cms\Block $block
 */

?>

<div class="media px-8 py-4 bg-primary-100 border-l-3 border-primary-600 relative">
    <div class="-ml-px absolute left-0 top-3 text-primary-600 transform-gpu -translate-x-1/2">
        <?= useSymbol('information-circle-filled', 'w-8 h-8') ?>
    </div>
    <?= $block->text()->kti() ?>
</div>

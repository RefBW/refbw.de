<?php

/**
 * @var \Kirby\Cms\Block $block
 */

if ($block->citation()->isNotEmpty()) :

?>
<blockquote
    class="media group my-4 border-3 border-dashed border-primary-600 hover:border-secondary-500 transition rounded"
    x-data="{ isOpen: <?php e($block->isOpen()->toBool(), 'true', 'false') ?> }"
>
    <button
        class="px-6 py-4 w-full flex justify-between items-center text-primary-600 group-hover:text-secondary-500 transition"
        @click="isOpen = !isOpen"
    >
        <h3 class="font-semibold text-xl text-primary-600 group-hover:text-secondary-500 transition">
            Erfahrungsbericht von <?= $block->citation() ?>
        </h3>
        <div class="flex items-center" x-show="!isOpen">
            <?= useSymbol('plus', 'w-8 h-8') ?>
        </div>
        <template x-if="isOpen" x-cloak>
            <?= useSymbol('minus', 'w-8 h-8') ?>
        </template>
    </button>
    <section class="px-6 pb-4 -mt-1 not-italic" x-show="isOpen" x-cloak>
        <?= $block->text()->kti() ?>
    </section>
</blockquote>
<?php endif ?>

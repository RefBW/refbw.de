<?php

/**
 * @var \Kirby\Cms\Block $block
 */

# Determine heading class
$class = $block->level() == 'h2'
    ? 'bookmark'
    : 'heading'
;

echo Html::tag($block->level()->or('h2'), Str::replace($block->text(), '&amp;', '&'), [
    'id' => Str::slug($block->text()),
    'class' => $class,
]);

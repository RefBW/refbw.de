<?php

/**
 * @var \Kirby\Cms\Block $block
 */

if ($block->url()->isNotEmpty()) {
    echo $block->url()->toEmbed();
}

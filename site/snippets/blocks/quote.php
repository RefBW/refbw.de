<?php

/**
 * @var \Kirby\Cms\Block $block
 */

?>
<blockquote class="media mx-6">
    <section class="italic">
        <?= $block->text() ?>
    </section>
    <?php if ($block->citation()->isNotEmpty()) : ?>
    <footer class="mt-2 flex justify-end font-medium text-base text-primary-600">
        <?= $block->citation() ?>
    </footer>
    <?php endif ?>
</blockquote>

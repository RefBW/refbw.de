<?php

/**
 * @var \Kirby\Cms\Block $block
 */

if ($image = $block->image()->toFile()) :

$link = $block->link();
$alt = $block->alt()->or($image->altAttribute());
$caption = $block->caption()->or($image->titleAttribute());

$thumb = $image->thumb('image');
$lightbox = $image->thumb('lightbox');

$attributes = [
    'class' => 'js-lightbox table rounded',
    'alt' => $alt->esc(),
    'style' => 'object-fit: cover; object-position: ' . $image->focusPercentageX() . '% ' . $image->focusPercentageY() . '%;',
    'data-src' => $thumb->url(),
    'data-lightbox' => $lightbox->url(),
    'data-width' => $lightbox->width(),
    'data-height' => $lightbox->height(),
    'data-lazyload' => true,
];

# If source information exists ..
if ($image->source()->isNotEmpty()) {
    # use it as `title` attribute
    $attributes['title'] = $image->source()->esc();
}

?>
<figure
    class="media flex justify-center rounded"
    x-data="lightbox"
>
    <div
        class="relative rounded cursor-pointer"
        @click="show()"
    >
        <?php if ($link->isNotEmpty()) : ?>
        <a href="<?= esc($link->toUrl()) ?>">
            <?= Html::img($thumb->url(), $attributes) ?>
        </a>
        <?php else : ?>
        <?= Html::img('', $attributes) ?>
        <?php endif ?>

        <?php if ($caption->isNotEmpty()) : ?>
        <figcaption class="absolute inset-0 px-16 flex flex-col justify-center items-center font-serif text-2xl text-white text-center bg-secondary-500 opacity-0 hover:opacity-90 transition rounded">
            <?= useSymbol('camera', 'w-16 h-16 mb-4') ?>
            <?= $caption ?>
        </figcaption>
        <?php endif ?>
    </div>
</figure>
<?php endif ?>

<div class="sidebar-item">
    <?php snippet('components/link', ['url' => 'mailto:post@refbw.de', 'title' => 'Kontakt per Mail', 'icon' => 'mail', 'size' => 'lg']) ?>
    <?php snippet('components/link', ['url' => $page->url(), 'title' => 'Filter zurücksetzen', 'icon' => 'filter', 'size' => 'lg']) ?>
</div>

<?php snippet('modules/base/filters', compact('filters', 'tagName')) ?>

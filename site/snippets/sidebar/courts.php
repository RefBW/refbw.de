<div class="sidebar-item">
    <?php foreach (['Karlsruhe', 'Stuttgart'] as $olg) : ?>
    <?php snippet('components/link', ['url' => sprintf('https://oberlandesgericht-%s.justiz-bw.de', Str::lower($olg)), 'title' => sprintf('zum OLG %s', $olg), 'icon' => 'library', 'size' => 'lg']) ?>
    <?php endforeach ?>
</div>

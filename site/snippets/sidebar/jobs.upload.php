<div class="sidebar-item">
    <?php if ($child = $page->parent()->find('stellenangebote-fuer-referendar-innen-gesucht')) snippet('components/link', ['url' => $child->url(), 'title' => 'Beispielseite', 'icon' => 'newspaper', 'size' => 'lg']) ?>
    <?php snippet('components/link', ['url' => 'mailto:post@refbw.de', 'title' => 'Kontakt per Mail', 'icon' => 'mail', 'size' => 'lg']) ?>
</div>

<?php snippet('modules/base/downloads', compact('downloads')) ?>

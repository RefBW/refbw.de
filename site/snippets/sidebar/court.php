<div class="sidebar-item">
    <?php snippet('components/link', ['url' => $link->toUrl(), 'title' => 'zum Landgericht', 'icon' => 'library', 'size' => 'lg']) ?>
    <?php snippet('components/link', ['url' => 'mailto:' . Str::slug($page->title()) . '@refbw.de', 'title' => 'Kontakt per Mail', 'icon' => 'mail', 'size' => 'lg']) ?>
</div>

<?php snippet('modules/contact', ['social' => $social, 'heading' => 'Für Referendar:innen vor Ort']) ?>
<?php snippet('modules/base/links', ['links' => $courts, 'heading' => 'Amtsgerichte']) ?>
<?php snippet('modules/base/links', ['links' => $prosecution, 'heading' => 'Staatsanwaltschaften']) ?>
<?php snippet('modules/base/links', ['links' => $references]) ?>
<?php snippet('modules/base/downloads', compact('downloads')) ?>

<?php if ($offerer = $page->getOfferer()) : ?>
<?php if ($logo = $offerer->logo()->toFile()) : ?>
<div class="sidebar-item">
    <img src="<?= $logo->resize(260)->url() ?>" title="<?= $page->offerer() ?>">
</div>
<?php endif ?>
<div class="sidebar-item">
    <h5 class="heading"><?= $offerer->title() ?></h5>
    <?php if ($offerer->text()->isNotEmpty()) : ?>
    <p class="text text-base">
        <?= $offerer->text()->kti() ?>
    </p>
    <?php endif ?>
</div>
<?php endif ?>

<?php snippet('modules/contact', ['social' => $social, 'homepage' => $page->getHomepage(), 'email' => $page->getEmail()]) ?>
<?php snippet('modules/base/downloads', compact('downloads')) ?>

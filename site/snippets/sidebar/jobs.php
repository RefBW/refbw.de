<div class="sidebar-item">
    <?php if ($child = $page->find('angebot-aufgeben')) snippet('components/link', ['url' => $child->url(), 'title' => 'Angebot aufgeben', 'icon' => 'newspaper', 'size' => 'lg']) ?>
    <?php snippet('components/link', ['url' => 'mailto:post@refbw.de', 'title' => 'Kontakt per Mail', 'icon' => 'mail', 'size' => 'lg']) ?>
    <?php snippet('components/link', ['url' => $page->url(), 'title' => 'Filter zurücksetzen', 'icon' => 'filter', 'size' => 'lg']) ?>
</div>

<?php snippet('modules/base/filters', ['filters' => $filters['period'], 'heading' => 'Abschnitt', 'isOpen' => true]) ?>
<?php snippet('modules/base/filters', ['filters' => $filters['tags'], 'heading' => 'Rechtsgebiete', 'isOpen' => false, 'tagName' => 'thema']) ?>
<?php snippet('modules/base/filters', ['filters' => $filters['locations'], 'heading' => 'Standort', 'isOpen' => false]) ?>
<?php snippet('modules/base/filters', ['filters' => $filters['misc'], 'heading' => 'Sonstiges', 'isOpen' => false]) ?>

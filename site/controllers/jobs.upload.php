<?php

use Kirby\Cms\App;
use Kirby\Cms\Page;
use Kirby\Filesystem\F;

use Uniform\Form;


return function (App $kirby, Page $page)
{
    $form = new Form([
        'subject' => [
            'rules' => ['required'],
            'message' => 'Bitte gib den Titel der zu vergebenden Stelle an.',
        ],
        'email' => [
            'rules' => ['required', 'email'],
            'message' => 'Bitte gib eine Email-Kontaktadresse an.',
        ],
        'tags' => [],
        'locations' => [],
        'message' => [
            'rules' => ['required'],
            'message' => 'Bitte fülle das Nachrichtenfeld aus.',
        ],
        'about' => [],
        'file' => [
            'rules' => [
                'file', 'mime' => ['application/pdf'],
                'filesize' => 5 * 1024,  # 5 MB
            ],
            'message' => 'Eine PDF-Datei darf nicht größer sein als 5 MB.',
        ],
        'logo' => [
            'rules' => [
                'file', 'mime' => ['image/jpeg', 'image/png'],
                'filesize' => 2 * 1024,  # 5 MB
            ],
            'message' => 'Eine Logo-Datei darf nicht größer sein als 2 MB.',
        ],
    ]);

    if ($kirby->request()->is('POST')) {
        # Call security
        $form->honeypotGuard()->simplecaptchaGuard()->validate();

        if ($form->success()) {
            # Save form data
            # (1) Create directory & log data
            $dataDir = $kirby->root('logs') . '/jobs';

            if (Dir::make($dataDir)) {
                # Build unique token from current time & email
                $timestamp = date('Y-m-d_H-i-s');
                $hash = md5($timestamp . $form->data('email'));

                # Log data
                $form->logAction([
                    'template' => 'jobs/log',
                    'file' => sprintf('%s/%s-%s.json', $dataDir, $timestamp, $hash),
                ]);
            }

            $attachments = [];

            # Check for uploaded files
            foreach ([$form->data('file'), $form->data('logo')] as $upload) {
                # If field unused ..
                if (empty($upload['name'])) {
                    # .. skip it
                    continue;
                }

                # Prepare uploaded file
                $file = $upload['tmp_name'];
                $temp = pathinfo($file);
                $filename = $temp['dirname'] . '/' . F::safeName($upload['name']);

                if (rename($upload['tmp_name'], $filename)) {
                    $file = $filename;
                }

                $attachments[] = $file;
            }

            # (2) Send data via email ..
            if (!empty($attachments)) {
                try {
                    # Attach file & send email
                    $kirby->email([
                        'from' => 'mailer@refbw.de',
                        'to' => 'webmaster@refbw.de',
                        'replyTo' => $form->data('email'),
                        'subject' => 'Neue Anfrage von ' . $form->data('email'),
                        'template' => 'email',
                        'data' => $form->data(),
                        'attachments' => $attachments,
                    ]);
                }

                catch (Exception $e) {
                    $form->fail('file', $e->getMessage());
                }
            }

            else {
                try {
                    $form->emailAction([
                        'from' => 'mailer@refbw.de',
                        'to' => 'webmaster@refbw.de',
                        'replyTo' => $form->data('email'),
                        'subject' => 'Neue Anfrage von {{ email }}',
                        'template' => 'email',
                    ]);
                } catch (Exception $e) {}
            }

            # Redirect to homepage
            $page->redirectPage()->go();
        }
    }

    # Fetch sidebar content
    $social = $page->social()->toStructure();
    $downloads = $page->downloads()->toStructure();

    return compact('form', 'social', 'downloads');
};

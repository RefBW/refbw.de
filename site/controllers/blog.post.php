<?php

use Kirby\Cms\Page;


return function(Page $page)
{
    # Post navigation
    $nav = [
        'hasLeft' => $page->hasNextListed(),
        'hasRight' => $page->hasPrevListed(),
    ];

    if ($nav['hasLeft']) {
        $nav['leftURL'] = $page->nextListed()->url();
        $nav['leftTitle'] = $page->nextListed()->heading()->or($page->nextListed()->title());
    }

    if ($nav['hasRight']) {
        $nav['rightURL'] = $page->prevListed()->url();
        $nav['rightTitle'] = $page->prevListed()->heading()->or($page->prevListed()->title());
    }

    # Fetch sidebar content
    $references = $page->references()->toStructure();
    $downloads = $page->downloads()->toStructure();

    return compact('nav', 'references', 'downloads');
};

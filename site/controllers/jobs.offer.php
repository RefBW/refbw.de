<?php

use Kirby\Cms\App;
use Kirby\Cms\Page;


return function(App $kirby, Page $page)
{
    # Retrieve job offers
    $jobs = $kirby->collection('jobs/active');

    # Post navigation
    $nav = [
        'hasLeft' => $page->hasNextListed($jobs),
        'hasRight' => $page->hasPrevListed($jobs),
    ];

    if ($nav['hasLeft']) {
        $nav['leftURL'] = $page->nextListed($jobs)->url();
        $nav['leftTitle'] = 'Vorheriges Angebot';
    }

    if ($nav['hasRight']) {
        $nav['rightURL'] = $page->prevListed($jobs)->url();
        $nav['rightTitle'] = 'Nächstes Angebot';
    }

    # Fetch sidebar content
    $social = $page->social()->or($page->getOfferer()->social())->toStructure();
    $downloads = $page->downloads()->toStructure();

    return compact('nav', 'social', 'downloads');
};

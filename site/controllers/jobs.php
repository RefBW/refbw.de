<?php

use Kirby\Cms\App;
use Kirby\Cms\Page;


return function(App $kirby, Page $page)
{
    # Fetch all jobs
    $jobs = $kirby->collection('jobs/active');

    # Map query parameter & field names
    $mapping = [
        'period' => 'abschnitt',
        'tags' => 'thema',
        'locations' => 'standort',
        'misc' => 'sonstiges',
    ];

    # Filter job offers by query (if present)
    foreach ($mapping as $field => $tagName) {
        if ($tag = param($tagName)) {
            $jobs = $jobs->filterBy($field, $tag, ',');
        }
    }

    # Create data array
    $filters = [];

    # Fetch available filters
    foreach ($mapping as $field => $tagName) {
        # Treat checkboxes differently by ..
        if ($field == 'period') {
            # .. using its field options
            $filters[$field] = [];

            if ($job = $jobs->first()) {
                $filters[$field] = $job->blueprint()->field($field)['options'];
            }

            # Move on
            continue;
        }

        # Retrieve field values
        $data = $jobs->pluck($field, ',', true);

        # Store sorted data (respecting umlauts)
        asort($data, SORT_LOCALE_STRING);
        $filters[$field] = $data;
    }

    # Apply pagination
    $perPage = $page->perpage()->int();
    $jobs = $jobs->paginate(($perPage >= 1) ? $perPage : 5);
    $pagination = $jobs->pagination();

    # Jobs navigation
    $nav = [
        'hasLeft' => $pagination->hasPrevPage(),
        'leftTitle' => 'Neuere Angebote',
        'leftEmpty' => 'Neuere Angebote',
        'hasRight' => $pagination->hasNextPage(),
        'rightTitle' => 'Frühere Angebote',
        'rightEmpty' => 'Frühere Angebote',
    ];

    if ($nav['hasLeft']) {
        $nav['leftURL'] = $pagination->prevPageURL();
    }

    if ($nav['hasRight']) {
        $nav['rightURL'] = $pagination->nextPageURL();
    }

    return compact('jobs', 'pagination', 'nav', 'filters');
};

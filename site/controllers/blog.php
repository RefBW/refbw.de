<?php

use Kirby\Cms\App;
use Kirby\Cms\Page;
use Kirby\Toolkit\A;


return function(App $kirby, Page $page)
{
    # Fetch all posts
    $posts = $kirby->collection('posts');

    # Filter by tag (if present)
    $tagName = 'thema';
    $field = 'tags';

    if ($tag = param($tagName)) {
        $posts = $posts->filterBy($field, $tag, ',');
    }

    # Fetch available filters & sort them (respecting umlauts)
    $filters = $posts->pluck($field, ',', true);
    asort($filters, SORT_LOCALE_STRING);

    # Apply pagination
    $perPage = $page->perpage()->int();
    $posts = $posts->paginate(($perPage >= 1) ? $perPage : 5);
    $pagination = $posts->pagination();

    # Blog navigation
    $nav = [
        'hasLeft' => $pagination->hasPrevPage(),
        'leftTitle' => 'Neuere Beiträge',
        'leftEmpty' => 'Neuere Beiträge',
        'hasRight' => $pagination->hasNextPage(),
        'rightTitle' => 'Frühere Beiträge',
        'rightEmpty' => 'Frühere Beiträge',
    ];

    if ($nav['hasLeft']) {
        $nav['leftURL'] = $pagination->prevPageURL();
    }

    if ($nav['hasRight']) {
        $nav['rightURL'] = $pagination->nextPageURL();
    }

    return compact('posts', 'pagination', 'nav', 'filters', 'tagName');
};

<?php

use Kirby\Cms\Page;


return function(Page $page)
{
    # Fetch sidebar content
    $references = $page->references()->toStructure();
    $downloads = $page->downloads()->toStructure();

    return compact('references', 'downloads');
};

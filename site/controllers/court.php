<?php

use Kirby\Cms\Page;


return function(Page $page)
{
    # Fetch reviews
    # TODO: Move to dedicated block
    $reviews = $page->reviews()->toStructure();

    # Fetch sidebar content
    $link = $page->website();
    $homepage = esc($page->homepage());
    $social = $page->social()->toStructure();
    $courts = $page->courts()->toStructure();
    $prosecution = $page->prosecution()->toStructure();
    $references = $page->references()->toStructure();
    $downloads = $page->downloads()->toStructure();

    return compact('reviews', 'link', 'homepage', 'social', 'courts', 'prosecution', 'references', 'downloads');
};

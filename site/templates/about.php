<?php layout() ?>

<?php slot() ?>
<h2 class="bookmark">Wer sind wir?</h2>
<p class="text">
    Diese Webseite wird von den Sprechervorständen Karlsruhe und Stuttgart bereitgestellt - aber was heißt das genau?
</p>
<p class="text">
    In Baden-Württemberg gibt es <strong class="font-medium">zwei Oberlandesgerichte</strong>: Karlsruhe und Stuttgart. Ein Sprechervorstand wird <strong class="font-medium">von den AG-Sprechern aller Landgerichte eines OLG-Bezirks</strong> auf einer mindestens einmal jährlich stattfindenden Sprecherkonferenz für vorzugsweise eine Amtszeit von einem Jahr <strong class="font-medium">gewählt</strong>. Die Sprechertreffen finden halbjährlich Anfang des Jahres und im Sommer statt.
</p>
<?php if ($delegates = $kirby->collection('delegates')) : ?>
<div class="mt-2 mb-8 flex flex-col lg:flex-row">
    <?php foreach ($delegates as $delegate) : ?>
    <a
        class="card group px-6 py-4 <?php e($delegates->indexOf($delegate) % 2, 'lg:ml-2', 'lg:mr-2') ?> mt-4 flex-1 flex flex-col justify-center"
        href="mailto:<?= $delegate->email() ?>"
    >
        <div class="mr-4 flex-none flex items-center text-primary-600 group-hover:text-white transition">
            <?= $site->symbol('mail', 'w-8 h-8') ?>
            <h3 class="ml-4 my-1 font-semibold text-xl"><?= $delegate->name() ?></h3>
        </div>
        <div class="flex-1">
            <p class="pl-12 text-base"><?= $delegate->byline() ?></p>
        </div>
    </a>
    <?php endforeach ?>
</div>
<?= $page->blocks()->toBlocks() ?>
<?php endif ?>
<?php endslot() ?>

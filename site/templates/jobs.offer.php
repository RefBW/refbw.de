<?php layout() ?>

<?php slot('header') ?>
<header class="px-8 pt-6 pb-8 text-white bg-primary-600">
    <?php snippet('layout/body/breadcrumbs') ?>

    <h1 class="text-4xl"><?= $page->heading()->or($page->title()) ?></h1>
    <div class="mt-4 flex items-center text-primary-900">
        <div class="flex-none flex items-center">
            <?= useSymbol('calendar', 'w-7 h-7') ?>
        </div>
        <div class="flex-1">
            <p class="ml-2 font-serif text-2xl">
                am <time datetime="<?= $page->date()->toDate('Y-m-d') ?>"><?= $page->date()->toDate('d.m.Y') ?></time> veröffentlicht
            </p>
        </div>
    </div>
</header>
<?php endslot() ?>

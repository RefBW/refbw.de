<?php layout() ?>

<?php slot() ?>
<?= $page->blocks()->toBlocks() ?>

<?php foreach (['Karlsruhe', 'Stuttgart'] as $olg) : ?>
<h2 id="<?= Str::lower($olg) ?>" class="bookmark">Oberlandesgericht <?= $olg ?></h2>
<div class="media">
    <div class="grid grid-cols-none md:grid-cols-2 gap-4">
        <?php foreach ($kirby->collection('courts')->filterBy('olg', $olg) as $court) : ?>
        <a
            class="card group px-6 py-4 flex flex-col justify-center"
            href="<?= $court->url() ?>"
        >
            <div class="flex-none flex items-center text-primary-600 group-hover:text-white transition">
                <?= $court->symbol('w-8 h-8') ?>
                <h3 class="ml-4 my-1 font-semibold text-xl"><?= $court->title() ?></h3>
            </div>
        </a>
        <?php endforeach ?>
    </div>
</div>
<?php endforeach ?>
<?php endslot() ?>

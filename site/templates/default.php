<?php layout() ?>

<?php slot() ?>
<?php if ($isPrivacyPage = $page->isPrivacyPage()) : ?>
<p class="text">
    <?= kti('<strong>Verantwortlich</strong> für den Betrieb dieser Website sind die Sprechervorstände der OLG-Bezirke Karlsruhe und Stuttgart:') ?>
</p>
<?php elseif ($isImprintPage = $page->isImprintPage()) : ?>
<p class="text">
    <?= kti('<strong>Diensteanbieter</strong> gemäß § 5 Abs. 1 TMG sowie inhaltlich verantwortlich gemäß (link: https://www.landesrecht-bw.de/jportal/?quelle=jlink&query=MedienStVtr+BW+%C2%A7+18&psml=bsbawueprod.psml&max=true text: § 18 Abs. 2 MStV) sind die Sprechervorstände der OLG-Bezirke Karlsruhe und Stuttgart:') ?>
</p>
<?php endif ?>

<?php if (($isPrivacyPage || $isImprintPage) && $delegates = $kirby->collection('delegates')) : ?>
<?php foreach ($delegates as $delegate) : ?>
<h3 class="heading"><?= $delegate->name() ?></h3>
<ul class="list">
    <?php foreach ($delegate->members()->toStructure() as $member) : ?>
    <li>
        <?= $member->name() ?> (Landgericht <?= $member->court()->toCourt() ?>)
    </li>
    <?php endforeach ?>
</ul>
<p class="text">
    Du erreichst ihn per Mail an <?= Str::replace($delegate->email(), '@refbw.de', ' [ät] refbw [punkt] de') ?> und per Brief über die Referendarabteilung des Oberlandesgerichts <?php e(Str::contains($delegate->email(), 'karlsruhe'), 'Karlsruhe, Hoffstraße 10, 76133 Karlsruhe', 'Stuttgart, Olgastraße 2, 70182 Stuttgart') ?>.
</p>
<?php endforeach ?>
<?php endif ?>

<?= $page->blocks()->toBlocks() ?>
<?php endslot() ?>

<?php layout() ?>

<?php slot() ?>
<h2 class="bookmark">
    <?php e($pagination->page() > 1, 'Seite ' . $pagination->page(), 'Erste Seite') ?>
</h2>
<?php snippet('articles', ['data' => $jobs]) ?>
<?php endslot() ?>

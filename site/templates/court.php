<?php layout() ?>

<?php slot() ?>
<?= $page->blocks()->toBlocks() ?>

<?php if ($reviews->isNotEmpty()) : ?>
<h2 id="erfahrungsberichte" class="bookmark">Erfahrungsberichte</h2>
<?php foreach ($reviews as $review) : ?>
<h3 id="<?= Str::slug($review->person()) ?>" class="heading" ><?= $review->person() ?></h3>
<?= $review->text()->kti() ?>
<?php endforeach ?>
<?php endif ?>
<?php endslot() ?>

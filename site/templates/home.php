<?php layout() ?>

<?php slot('header') ?>
<header class="px-8 py-16 flex justify-center text-white bg-primary-600">
    <h1 class="max-w-2xl text-4xl text-center leading-relaxed">
        <?= $page->heading()->or($page->title()) ?>
    </h1>
</header>
<?php endslot() ?>

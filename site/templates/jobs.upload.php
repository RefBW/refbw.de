<?php layout() ?>

<?php slot('page-footer') ?>
<h2 class="bookmark">Angebot aufgeben</h2>
<?php
    # Load contact form
    snippet('jobs/error', compact('form'));
    snippet('jobs/form');
?>
<?php endslot() ?>

<!DOCTYPE html>
<html class="no-js min-h-screen scroll-smooth" lang="de">
    <?php snippet('layout/head') ?>

    <body class="min-h-screen font-sans font-normal text-lg text-black bg-gray-300">
        <div class="container lg:max-w-6xl min-h-screen my-4 flex flex-col shadow-md relative rounded">
            <!-- SITE HEADER -->
            <?php snippet('layout/header') ?>

            <!-- BODY -->
            <?php snippet('layout/body') ?>

            <!-- SITE FOOTER -->
            <?php snippet('layout/footer') ?>
        </div>
    </body>
</html>

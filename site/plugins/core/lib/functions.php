<?php

use Kirby\Cms\App;
use Kirby\Http\Url;
use Kirby\Toolkit\F;
use Kirby\Toolkit\Str;


/**
 * Includes an SVG file by absolute or relative file path
 *
 * @param string|\Kirby\Cms\File $file
 * @return string|false
 */
function useSVG(?string $file, string $classes = '')
{
    # support for Kirby's file objects
    if (is_a($file, 'Kirby\Cms\File') === true && $file->extension() === 'svg') {
        return $file->read();
    }

    if (is_string($file) === false) {
        return false;
    }

    $extension = F::extension($file);

    # check for valid svg files
    if ($extension !== 'svg') {
        return false;
    }

    # try to convert relative paths to absolute
    if (file_exists($file) === false) {
        $root = App::instance()->root();
        $file = realpath($root . '/' . $file);
    }

    return Str::replace(F::read($file), '<svg', '<svg class="' . $classes . '"');
}


/**
 * Includes matching SVG file for given page slug
 *
 * @param string $slug Page slug
 * @param string $classes Custom classes
 * @return string
 */
function useSymbol(?string $slug = null, string $classes = ''): string
{
    $icon = $slug ?? 'question-mark-circle';

    $symbols = [
        # Page identifiers
        'landgerichte' => 'library',
        'referendariat' => 'scale',
        'zweites-staatsexamen' => 'academic-cap',
        'stellenangebote' => 'newspaper',
        'tipps-und-tricks' => 'cursor-click',
        'aktuelles' => 'annotation',
        'ueber-uns' => 'chat',
        'datenschutz' => 'fingerprint',
        'impressum' => 'information-circle',

        # Custom identifiers
        'panel' => 'lock-closed',
        'homepage' => 'home',
        'facebook' => 'thumbs-up',
        'instagram' => 'instagram-feather',
        'twitter' => 'twitter-feather',
        'mastodon' => 'mastodon-simple',
    ];

    if (in_array($slug, array_keys($symbols))) {
        $icon = $symbols[$slug];
    }

    return useSVG(sprintf('%s/icons/%s.svg', kirby()->root('assets'), $icon), $classes);
}


/**
 * Checks whether given URL is external
 *
 * @param string $url URL
 * @return bool
 */
function isExternal(string $url): bool
{
    return Url::stripPath(Url::to($url)) !== Url::stripPath(site()->url());
}

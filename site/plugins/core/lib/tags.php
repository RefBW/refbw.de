<?php

use Kirby\Cms\KirbyTag;
use Kirby\Http\Url;
use Kirby\Toolkit\Html;


return [
    /**
     * Core extensions
     */
    'link' => [
        # Default attributes:
        # - class
        # - lang
        # - rel
        # - role
        # - target
        # - text
        # - title
        'attr' => KirbyTag::$types['link']['attr'],


        /**
         * Creates `a` tag
         *
         * @param Kirby\Cms\KirbyTag $tag
         * @return string
         */
        'html' => function(KirbyTag $tag): string
        {
            if (empty($tag->lang) === false) {
                $tag->value = Url::to($tag->value, $tag->lang);
            }

            # Check if URL is external
            if (isExternal($tag->value)) {
                $tag->target = '_blank';
            }

            return Html::a($tag->value, $tag->text, [
                'rel'    => $tag->rel,
                'class'  => $tag->class,
                'role'   => $tag->role,
                'title'  => $tag->title,
                'target' => $tag->target,
            ]);
        },
    ],
];

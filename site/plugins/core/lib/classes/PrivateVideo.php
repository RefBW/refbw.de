<?php

use Exception;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\ClientException as GuzzleException;


/**
 * Class PrivateVideo
 */
class PrivateVideo
{
    /**
     * Properties
     */

    /**
     * HTTP client
     *
     * @var \GuzzleHttp\Client
     */
    private $client = null;


    /**
     * User-Agent used when downloading thumbnail images
     *
     * @var string
     */
    private $userAgent = 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:45.0) Gecko/20100101 Firefox/45.0';


    /**
     * Video platform provider
     *
     * @var string
     */
    public $provider = null;


    /**
     * Video ID
     *
     * @var string
     */
    public $id = null;


    /**
     * Video embed URL
     *
     * @var string
     */
    public $src = null;


    /**
     * Thumbnail image URL
     *
     * @var string
     */
    public $thumb = null;


    /**
     * Whether video URL is playlist
     *
     * @var bool
     */
    public $hasPlaylist = false;


    /**
     * Constructor
     */

    public function __construct(string $string = 'https://youtu.be/dQw4w9WgXcQ')
    {
        # Initialize HTTP client
        $defaults = ['headers' => ['User-Agent' => $this->userAgent]];
        $this->client = new GuzzleClient(['defaults' => $defaults]);

        # Sanitize input
        $string = stripslashes(trim($string));
        $string = htmlspecialchars_decode($string);

        # Check if string contains iframe / embed code
        if (strpos($string, 'iframe') !== false) {
            $srcPattern = '/src="(.*)?"/isU';

            if (preg_match($srcPattern, $string, $matches)) {
                $string = trim($matches[1]);
            }
        }

        # Parse given string & extract video data
        $this->src = $this->parse($string);
    }


    /**
     * Methods
     */

    /**
     * Parses video URL string
     *
     * @param string $string - String containing video ID (or not)
     * @throws Exception
     * @return string
     */
    protected function parse(string $string): string {
        if ($youtube = $this->youtube($string)) {
            return $youtube;
        }

        if ($vimeo = $this->vimeo($string)) {
            return $vimeo;
        }

        throw new \Exception('Invalid video source');
    }


    /**
     * Parses string for information about YouTube URL
     *
     * @param string $string - String containing video ID (or not)
     * @return string|null
     */
    public function youtube(string $string)
    {
        $src = null;

        # (1) Extract YouTube video ID
        # See http://stackoverflow.com/questions/2936467/parse-youtube-video-id-using-preg-match
        $youtubePattern = '%(?:youtube(?:-nocookie)?\.com/(?:[\w\-?&!#=,;]+/[\w\-?&!#=/,;]+/|(?:v|e(?:mbed)?)/|[\w\-?&!#=,;]*[?&]v=)|youtu\.be/)([\w-]{11})(?:[^\w-]|\Z)%i';

        if (preg_match($youtubePattern, $string, $matches)) {
            $this->provider = 'youtube';
            $this->id = $matches[1];

            # (2) Extract YouTube playlist ID (if available)
            # See https://stackoverflow.com/questions/5115233/fetching-youtube-playlist-id-with-php-regex
            $playlist = null;
            $parts = parse_url($string);

            if (isset($parts['query'])) {
                parse_str($parts['query'], $query);

                if (isset($query['list'])) {
                    # Store playlist ID & toggle playlist
                    $playlist = $query['list'];
                    $this->hasPlaylist = true;
                }
            }

            # (3) Build embed URL
            $src = 'https://www.youtube-nocookie.com/embed/' . $this->id . '?autoplay=1';

            if ($playlist !== null) {
                $src = 'https://www.youtube-nocookie.com/embed/videoseries?list=' . $playlist . '&autoplay=1';
            }
        }

        return $src;
    }


    /**
     * Parses string for information about Vimeo URL
     *
     * @param string $string - String containing video ID (or not)
     * @return string|null
     */
    public function vimeo(string $string)
    {
        $src = null;

        # (1) Extract Vimeo video ID
        # See https://stackoverflow.com/questions/272361/how-can-i-handle-the-warning-of-file-get-contents-function-in-php
        # TODO: Guzzle
        if (($response = @file_get_contents('https://vimeo.com/api/oembed.json?url=' . $string)) !== false) {
            $this->id = json_decode($response)->video_id;

            # (3) Build embed URL
            $src = 'https://player.vimeo.com/video/' . $this->id . '?dnt=1&autoplay=1';

            # (3) Build thumbnail URL
            # See https://stackoverflow.com/questions/1361149/get-img-thumbnails-from-vimeo
            $thumbnail = json_decode($response)->thumbnail_url;
            $thumbnail = substr($thumbnail, strrpos($thumbnail, '/') + 1);
            $thumbnail = substr($thumbnail, 0, strrpos($thumbnail, '_'));

            $this->thumb = 'https://i.vimeocdn.com/video/' . $thumbnail . '_1280x720.jpg';
        }

        return $src;
    }


    /**
     * Grabs thumbnail image URL
     *
     * @return string
     */
    public function thumbnail(): string
    {
        # With Vimeo, thumb is already defined
        if ($this->thumb !== null) {
            return $this->thumb;
        }

        $base = 'https://i.ytimg.com/vi/' . $this->id;
        $thumbs = [
            $base . '/maxresdefault.jpg',
            $base . '/hqdefault.jpg',
            $base . '/default.jpg',
        ];

        foreach ($thumbs as $thumb) {
            try {
                # Check if thumbnail size exists
                $this->client->get($thumb);

                return $this->thumb = $thumb;
            } catch (GuzzleException $e) {
                continue;
            }
        }
    }


    /**
     * Downloads thumbnail image
     *
     * @param string $filePath - Filepath for downloading the thumbnail image
     * @param bool $overwrite - Whether an existing file should be overwritten
     * @return bool
     */
    public function download(string $filePath, bool $overwrite = false): bool
    {
        # Skip if file exists & overwriting it is disabled
        if (file_exists($filePath) && !$overwrite) {
            return true;
        }

        # Start download
        $success = false;

        if ($handle = fopen($filePath, 'w')) {
            try {
                $response = $this->client->get($this->thumbnail(), ['sink' => $handle]);
                $success = true;
            } catch (GuzzleException $e) {}
        }

        return $success;
    }


    /**
     * Exports data
     *
     * @return array
     */
    public function export(): array
    {
        return [
            'id'       => $this->id,
            'src'      => $this->src,
            'thumb'    => $this->thumb,
            'provider' => $this->provider,
        ];
    }
}

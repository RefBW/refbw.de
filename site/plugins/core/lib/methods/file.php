<?php

use Kirby\Cms\FileVersion;


return [
    /**
     * Fetches 'OpenGraph' image
     *
     * @return \Kirby\Cms\FileVersion
     */
    'toOpenGraph' => function (): FileVersion
    {
        return $this->focusPreset('openGraph');
    },


    /**
     * Fetches 'TwitterCard' image
     *
     * @return \Kirby\Cms\FileVersion
     */
    'toTwitterCard' => function (): FileVersion
    {
        return $this->focusPreset('twitterCard');
    },
];

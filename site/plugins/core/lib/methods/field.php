<?php

use Kirby\Cms\Field;
use Kirby\Toolkit\Str;


return [
    /**
     * Converts regional court identifier to its full name
     *
     * @param Kirby\Cms\Field
     * @return string
     */
    'toCourt' => function(Field $field): string
    {
        return kirby()->collection('courts')->findBy('uid', $field->value)->title();
    },


    /**
     * Checks whether given URL is external
     *
     * @param Kirby\Cms\Field
     * @return bool
     */
    'isExternal' => function(Field $field): string
    {
        return isExternal($field->value);
    },


    /**
     * Formats price
     *
     * @param Kirby\Cms\Field
     * @return string
     */
    'toEuro' => function(Field $field): string
    {
        return Str::replace(sprintf('%.2f', $field->toFloat()), '.', ',');
    },


    /**
     * Video embeds
     */

    /**
     * Creates video embed thumbnail
     *
     * @param Kirby\Cms\Field $field
     * @param string $fileName Thumbnail filename
     * @return Kirby\Cms\File
     */
    'toVideoThumbnail' => function(Field $field, ?string $fileName = null): File
    {
        return $field->model()->toVideoThumbnail($field->value, $fileName);
    },


    /**
     * Creates `iframe` element
     *
     * @param Kirby\Cms\Field $field
     * @param array $options HTML attributes
     * @return string
     */
    'toIframe' => function(Field $field, array $options = [])
    {
        return $field->model()->toIframe($field->value, $options);
    },


    /**
     * Creates video embed
     *
     * @param Kirby\Cms\Field $field
     * @param array $options HTML attributes
     * @return string
     */
    'toEmbed' => function(Field $field, array $options = [])
    {
        return $field->model()->toEmbed($field->value, $options);
    },
];

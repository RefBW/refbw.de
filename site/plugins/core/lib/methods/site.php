<?php

use Kirby\Cms\File;
use Kirby\Cms\Pages;


return [
    /**
     * Provides given symbol
     *
     * string $symbol Symbol identifier
     * string $classes Custom classes
     * @return string SVG string
     */
    'symbol' => function(string $symbol, string $classes = ''): string
    {
        return useSymbol($symbol, $classes);
    },


    /**
     * Provides header image
     *
     * @return Kirby\Cms\File
     */
    'hero' => function(): File
    {
        return $this->image('header_ourselp@unsplash.jpg');
    },


    /**
     * Fetches pages for top navigation
     *
     * @return Kirby\Cms\Pages
     */
    'topNav' => function(): Pages
    {
        return new Pages([
            page('kontakt'),
            page('ueber-uns'),
        ]);
    },


    /**
     * Fetches pages for main navigation
     *
     * @return Kirby\Cms\Pages
     */
    'mainNav' => function(): Pages
    {
        return site()->children()->listed();
    },


    /**
     * Fetches pages for bottom navigation
     *
     * @return Kirby\Cms\Pages
     */
    'bottomNav' => function(): Pages
    {
        return new Pages([
            page('datenschutz'),
            page('impressum'),
        ]);
    },
];

<?php

use Kirby\Cms\File;
use Kirby\Cms\Page;
use Kirby\Filesystem\F;
use Kirby\Toolkit\A;
use Kirby\Toolkit\Html;


return [
    /**
     * Snippet shortcut
     *
     * @param string $snippet Name of snippet or content area
     * @return string|null
     */
    'snippet' => function (string $snippet): ?string
    {
        return snippet(sprintf('%s/%s', $snippet, $this->intendedTemplate()), return: true);
    },


    /**
     * Fetches cover image, with hero as fallback
     *
     * @return Kirby\Cms\File
     */
    'cover' => function (): File
    {
        # Fetch suitable images
        $images = $this->images()->filterBy('template', 'image');

        # If images are present ..
        if ($images->count() > 0) {
            # .. attempt to select cover from landscapes first ..
            $landscape = $images->filterBy(function($image) {
                return $image->isLandscape();
            });

            # .. but fallback to portraits
            $images = $landscape->isNotEmpty() ? $landscape : $images;

            # Provide widest image as cover
            return $images->sortBy('isCover', 'desc', 'width', 'desc')->first();
        }

        return site()->hero();
    },


    /**
     * Fetches page summary
     *
     * @param int $length Maximum number of characters
     * @return string
     */
    'summary' => function (?int $length = null): string
    {
        # If description is available ..
        if ($text = $this->text()) {
            # .. use it
            if ($text->isNotEmpty()) return $text->kti();
        }

        # Fetch 'text' blocks
        $blocks = $this->blocks()->toBlocks()->filterBy('type', 'text');

        # If at least one is available ..
        if ($blocks->count() > 0) {
            # .. use it
            return $blocks->first()->excerpt($length ?? 155);
        }

        # If currently on 'court' page ..
        if ($this->template() == 'court') {
            $reviews = $this->reviews()->toStructure();

            # .. and at least one review is available ..
            if ($reviews->isNotEmpty()) {
                # .. use it
                return $reviews->first()->text()->excerpt($length ?? 155);
            }
        }

        return '';
    },


    /**
     * Fetches 'real' page for top-level pages
     *
     * @return Kirby\Cms\Page
     */
    'aliasPage' => function(): Page
    {
        if (in_array($this->template(), ['blog', 'courts', 'jobs'])) {
            return $this;
        }

        return $this->children()->listed()->first();
    },


    /**
     * Checks if currently on 'data privacy' page
     *
     * @return bool
     */
    'isPrivacyPage' => function(): bool
    {
        return $this->intendedTemplate() == 'privacy';
    },


    /**
     * Checks if currently on 'imprint' page
     *
     * @return bool
     */
    'isImprintPage' => function(): bool
    {
        return $this->intendedTemplate() == 'imprint';
    },


    /**
     * Fetches page's root page
     *
     * @return Kirby\Cms\Page
     */
    'indexPage' => function(): Kirby\Cms\Page
    {
        $page = $this;

        while ($page->depth() > 1) {
            $page = $page->parent();
        }

        return $page;
    },


    /**
     * Provides symbol of page's root page
     *
     * @param string $classes Custom classes
     * @return string SVG string
     */
    'symbol' => function(string $classes = ''): string
    {
        return useSymbol($this->indexPage()->uid(), $classes);
    },


    /**
     * Generates byline with page details
     *
     * @return string
     */
    'byline' => function(): string
    {
        # If 'linkable' children are unavailable ..
        if (!$this->hasLinkableChildren()) {
            # .. return empty string
            return '';
        }

        # Create data array
        $array = [];

        # Iterate over 'linkable' child pages ..
        foreach ($this->linkableChildren() as $page) {
            # .. storing their title name
            $array[] = $page->title();
        }

        # Combine page titles
        return A::join($array, ', ');
    },


    /**
     * Checks whether 'linkable' children exist
     *
     * @return bool
     */
    'hasLinkableChildren' => function(): bool
    {
        return $this->linkableChildren()->count() > 0;
    },


    /**
     * Collects 'linkable' children
     *
     * @return Kirby\Cms\Pages
     */
    'linkableChildren' => function(): Kirby\Cms\Pages
    {
        return $this->children()->listed()->filterBy('intendedTemplate', 'article');
    },


    /**
     * Fetches items for tabs of main navigation
     *
     * @return array
     */
    'tabbedNav' => function(): array
    {
        return array_map(function(array $page) {
            return [
                'title' => $page['content']['title'],
                'url' => $page['url'],
            ];
        }, $this->linkableChildren()->toArray());
    },


    /**
     * Determines wether tab of given URL is currently active
     *
     * @param string $url Comparable URL
     * @return bool
     */
    'isActiveTab' => function(string $url): bool
    {
        # Compare given URL to current page URL using active query parameters
        return $url == url($this->id(), ['params' => params()]);
    },


    /**
     * Checks whether pagelinks should be displayed
     *
     * @return bool
     */
    'hasPageLinks' => function(): bool
    {
        # Allow pages by ..
        # (1) .. template
        if (in_array($this->intendedTemplate(), [
            'article',
            'court',
        ])) {
            return true;
        }

        # (2) .. page identifier
        return $this->isHomeOrErrorPage();
    },


    /**
     * Checks whether table of contents should be displayed
     *
     * @return bool
     */
    'hasToc' => function(): bool
    {
        # Block pages by ..
        # (1) .. template
        if (in_array($this->intendedTemplate(), ['article', 'court'])) {
            return true;
        }

        # (2) .. page identifier
        return in_array($this->uid(), []);
    },


    /**
     * Fetches headings for table of contents
     *
     * @return array
     */
    'toc' => function (): array
    {
        $toc = [];

        foreach ($this->blocks()->toBlocks()->filterBy('type', 'heading')->pluck('content') as $heading) {
            $toc[] = [
                'level' => $heading->level()->value(),
                'text' => $heading->text()->value(),
            ];
        }

        return $toc;
    },


    /**
     * Video embeds
     */

    /**
     * Creates video embed thumbnail
     *
     * @param string $url Video URL
     * @param string $fileName Thumbnail filename
     * @return Kirby\Cms\File
     */
    'toVideoThumbnail' => function(string $url, ?string $fileName = null): File
    {
        $video = new PrivateVideo($url);

        # Build filepath
        if ($fileName === null) {
            $fileName = F::safeName($video->provider . '-' . $video->id . '.jpg');
        }

        $filePath = $this->root() . '/' . $fileName;

        # Download placeholder image (if it doesn't exist already)
        if (!F::exists($filePath)) {
            $download = $video->download($filePath);

            if ($download === true) {
                # Create file object + metadata
                $image = new File([
                    'filename' => $fileName,
                    'parent' => $this,
                ]);

                # Update template of downloaded image
                $image->update(['template' => option('refbw.video.template', 'image')]);
            }

            else {
                # Define fallback image
                $image = $site->videoFallback()->toFile();
            }
        }

        else {
            $image = $this->image($fileName);
        }

        return $image;
    },


    /**
     * Creates `iframe` element
     *
     * @param string $url Video URL
     * @param array $options HTML attributes
     * @return string
     */
    'toIframe' => function(string $url, array $options = []): string
    {
        # Determine options
        $options = A::update(option('refbw.video.options', ['x-ref' => 'iframeElement']), $options);

        return Html::iframe('', A::update(['data-src' => (new PrivateVideo($url))->src], $options));
    },


    /**
     * Creates video embed
     *
     * @param string $url Video URL
     * @param array $options HTML attributes
     */
    'toEmbed' => function(string $url, array $options = [])
    {
        # Determine options
        $options = A::update(option('refbw.video.options', ['x-ref' => 'iframeElement']), $options);

        return snippet('embed/video', ['url' => $url, 'data' => $this, 'options' => $options]);
    },
];

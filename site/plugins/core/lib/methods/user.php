<?php

use Kirby\Data\Json;
use Kirby\Toolkit\A;


return [
    /**
     * Checks whether user is Higher Regional Court speaker
     *
     * @return bool
     */
    'isLeader' => function (): bool
    {
        return $this->role() == 'leader';
    },


    /**
     * Checks whether user is Regional Court speaker
     *
     * @return bool
     */
    'isSpeaker' => function (): bool
    {
        return $this->role() == 'speaker';
    },


    /**
     * @param Kirby\Cms\Page|Kirby\Cms\Site $page
     * @return bool
     */
    'isAllowed' => function($page): bool
    {
        # Succeed early for ..
        # (1) .. admin accounts
        if ($this->isAdmin()) {
            return true;
        }

        # (2) .. Higher Regional Court speakers ..
        if ($this->isLeader()) {
            # .. unless editing page of regional court
            return $page->template() != 'court';
        }

        # (3) .. blog posts
        if ($page->template() == 'blog.post') {
            return true;
        }

        # Implement additional checks for the rest ..
        if ($page->template() == 'court') {
            # Enumerate authorized user IDs for each regional court
            $allowed = Json::read(kirby()->root('base') . '/users.json');

            # Use regional court as key
            $key = $page->slug();

            # Fail for unrecognized user ..
            if (!isset($allowed[$key])) {
                return false;
            }

            # .. otherwise check authorization
            return in_array($this->id(), $allowed[$key]);
        }

        return false;
    },


    /**
     * Generates byline with user details
     *
     * @return string
     */
    'byline' => function(): string
    {
        # Create data array
        $array = [];

        # Iterate over its members ..
        foreach ($this->members()->toStructure() as $member) {
            # .. storing their title name
            $array[] = sprintf('%s (LG %s)', $member->name(), $member->court()->toCourt());
        }

        # Combine page titles
        return A::join($array, ', ');
    },
];

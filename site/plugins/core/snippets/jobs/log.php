<?php

use Kirby\Cms\App;
use Kirby\Toolkit\A;


# Store data as pretty-printed JSON
echo json_encode(A::merge([
	'timestamp' => date('c'),
	'ipAddress' => App::instance()->visitor()->ip(),
	'userAgent' => App::instance()->visitor()->userAgent(),
], $data), JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);

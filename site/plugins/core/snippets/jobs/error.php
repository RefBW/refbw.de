<?php if (count($form->errors()) > 0) : ?>
<h2 class="heading">Fehler bei der Datenübermittlung</h2>
<p class="text">Leider hat beim Absenden etwas nicht geklappt:</p>
<ul class="list">
    <?php foreach ($form->errors() as $error) : ?>
    <li class="font-medium"><?= implode('<br>', $error) ?></li>
    <?php endforeach ?>
</ul>
<?php endif ?>

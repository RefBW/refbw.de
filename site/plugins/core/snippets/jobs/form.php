<form class="mt-8" method="POST" enctype="multipart/form-data">
    <div class="flex flex-col md:flex-row md:space-x-4">
        <div class="flex-1 mb-8 <?php e($form->error('subject'), 'form-error') ?>">
            <label for="subject" class="form-label">
                <div class="flex justify-between items-center">
                    <div>Stellenbezeichnung<span class="form-required">*</span></div>
                    <span class="form-required text-sm">* erforderlich</span>
                </div>
            </label>
            <input
                class="form-input"
                id="subject"
                name="subject"
                type="text"
                placeholder="Dein Stellenangebot"
                value="<?= $form->old('subject') ?>"
            >
        </div>
        <div class="flex-1 mb-8 <?php e($form->error('email'), 'form-error') ?>">
            <label for="email" class="form-label">
                <div class="flex justify-between items-center">
                    <div>Kontakt per Email<span class="form-required">*</span></div>
                    <span class="form-required text-sm">* erforderlich</span>
                </div>
            </label>
            <input
                class="form-input"
                id="email"
                name="email"
                type="email"
                placeholder="deine@mail.adresse"
                value="<?= $form->old('email') ?>"
            >
        </div>
    </div>
    <div class="flex flex-col md:flex-row md:space-x-4">
        <div class="flex-1 mb-8 <?php e($form->error('tags'), 'form-error') ?>">
            <label for="tags" class="form-label">Rechtsgebiete</label>
            <input
                class="form-input"
                id="tags"
                name="tags"
                type="text"
                placeholder="Recht1, Recht2"
                value="<?= $form->old('tags') ?>"
            >
        </div>
        <div class="flex-1 mb-8 <?php e($form->error('locations'), 'form-error') ?>">
            <label for="locations" class="form-label">Standorte</label>
            <input
                class="form-input"
                id="locations"
                name="locations"
                type="text"
                placeholder="Stadt1, Stadt2"
                value="<?= $form->old('locations') ?>"
            >
        </div>
    </div>
    <div class="mb-8 <?php e($form->error('message'), 'form-error') ?>">
        <label for="message" class="form-label">
            <div class="flex justify-between items-center">
                <div>Nachricht<span class="form-required">*</span></div>
                <span class="form-required text-sm">* erforderlich</span>
            </div>
        </label>
        <textarea
            class="form-input"
            id="message"
            name="message"
            rows="8"
            placeholder="Hier ist Platz für deine Stellenausschreibung sowie ergänzende Hinweise, zB zur angehängten PDF-Datei, oder eine Webseite bzw. 'Social Media'-Kanäle, auf die wir verweisen können oder sonstige Informationen .."
        ><?= $form->old('message') ?></textarea>
    </div>
    <div class="mb-8 <?php e($form->error('about'), 'form-error') ?>">
        <label for="about" class="form-label">Über den Anbieter</label>
        <textarea
            class="form-input"
            id="about"
            name="about"
            rows="4"
            placeholder="Neben der Stellenausschreibung soll auch der Anbieter dahinter kurz vorgestellt werden - ein solch kurzer Vorstellungstext kommt in dieses Feld .."
        ><?= $form->old('about') ?></textarea>
    </div>
    <div class="flex flex-col md:flex-row md:space-x-4">
        <div class="mb-8 md:w-1/2 <?php e($form->error('file'), 'form-error') ?>">
            <label for="file" class="form-label">PDF-Datei hochladen</label>
            <input
                id="file"
                class="form-input"
                type="file"
                name="file"
            >
        </div>
        <div class="mb-8 md:w-1/2 <?php e($form->error('logo'), 'form-error') ?>">
            <label for="logo" class="form-label">Logo hochladen</label>
            <input
                id="logo"
                class="form-input"
                type="file"
                name="logo"
            >
        </div>
    </div>
    <div class="mb-8 lg:pr-2 lg:w-1/2">
        <label for="captcha" class="form-label">
            <div class="flex justify-between items-center">
                <div>Captcha-Abfrage<span class="form-required">*</span></div>
                <span class="form-required text-sm">* erforderlich</span>
            </div>
        </label>
        <div class="flex flex-col sm:flex-row sm:space-x-2 space-y-4 sm:space-y-0">
            <div class="flex-none text-center">
                <?= simpleCaptcha(['class' => 'inline-block rounded']) ?>
            </div>
            <div class="flex-1">
                <?= simpleCaptchaField('captcha', ['class' => 'form-input']) ?>
            </div>
        </div>
    </div>
    <div>
        <?= csrf_field() ?>
        <?= honeypot_field() ?>
    </div>
    <input
        class="form-button"
        type="submit"
        value="Absenden"
    >
</form>

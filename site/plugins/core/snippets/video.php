<?php
    # Gather data & download thumbnail image
    $image = $data->toVideoThumbnail($url);
    $thumb = $image->thumb(option('refbw.video.preset', 'image'));
?>
<div class="media relative rounded" x-data="video">
    <button
        class="group w-full h-full flex justify-center items-center absolute inset-0 bg-primary-900 bg-opacity-90 hover:bg-opacity-75 transition rounded"
        @click="start()"
        @click.away="stop()"
        data-src="<?= (new PrivateVideo($url))->src ?>"
    >
        <?= useSymbol('eye', 'embed-svg is-on') ?>
        <?= useSymbol('eye-off', 'embed-svg is-off') ?>
        <span class="sr-only">Abspielen</span>
    </button>
    <img
        class="w-full h-auto rounded"
        :class="{ 'hidden': isPlaying }"
        src="<?= $thumb->url() ?>"
        title="<?= $image->titleAttribute() ?>"
        alt="<?= $image->altAttribute() ?>"
        width="<?= $thumb->width() ?>"
        height="<?= $thumb->height() ?>"
    >
    <div
        class="embed hidden"
        :class="{ 'hidden': !isPlaying }"
        x-show="isPlaying"
        x-transition:enter.opacity.500ms
        x-transition:leave.opacity.500ms
    >
        <?= $data->toIframe($url, $options) ?>
    </div>
</div>

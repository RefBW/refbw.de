<?php

use Kirby\Cms\App as Kirby;


# Load helper functions
require_once __DIR__ . '/lib/functions.php';

# Load classes
load(['privatevideo' => 'lib/classes/PrivateVideo.php'], __DIR__);


# Initialize plugin
Kirby::plugin('sprechervorstand/core', [
    # Options
    'options' => [
        # HTML attributes of `iframe` element
        'refbw.video.options' => [
            'allow' => 'autoplay; encrypted-media',
            'allowfullscreen' => true,
        ],
    ],

    # Methods
    # (1) Field methods
    'fieldMethods' => require_once __DIR__ . '/lib/methods/field.php',
    # See https://getkirby.com/docs/reference/plugins/extensions/field-methods

    # (2) Field methods
    'fileMethods' => require_once __DIR__ . '/lib/methods/file.php',
    # See https://getkirby.com/docs/reference/plugins/extensions/file-methods

    # (3) Page methods
    'pageMethods' => require_once __DIR__ . '/lib/methods/page.php',
    # See https://getkirby.com/docs/reference/plugins/extensions/page-methods

    # (4) Site methods
    'siteMethods' => require_once __DIR__ . '/lib/methods/site.php',
    # See https://getkirby.com/docs/reference/plugins/extensions/site-methods

    # (5) User methods
    'userMethods' => require_once __DIR__ . '/lib/methods/user.php',
    # See https://getkirby.com/docs/reference/plugins/extensions/user-methods

    # Templating
    # (1) Snippets
    # See https://getkirby.com/docs/reference/plugins/extensions/snippets
    'snippets' => [
        'embed/video' => __DIR__ . '/snippets/video.php',
        'jobs/error' => __DIR__ . '/snippets/jobs/error.php',
        'jobs/form' => __DIR__ . '/snippets/jobs/form.php',
    ],

    # (2) Templates
    # See https://getkirby.com/docs/reference/plugins/extensions/templates
    'templates' => [
        'jobs/log' => __DIR__ . '/snippets/jobs/log.php',
    ],

    # KirbyTags
    'tags' => require_once __DIR__ . '/lib/tags.php',
    # See https://getkirby.com/docs/reference/plugins/extensions/kirbytags
]);

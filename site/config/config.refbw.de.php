<?php

use Kirby\Cms\Page;


return [
    # Deactivate debug mode
    'debug' => false,

    # Activate Caching
    'cache' => [
        'pages' => [
            # Enable page cache
            'active' => true,


            /**
             * Determines pages to be ignored
             *
             * @param Kirby\Cms\Page $page
             * @return bool
             */
            'ignore' => function(Page $page): bool
            {
                return in_array($page->intendedTemplate(), ['jobs.upload']);
            },
        ],
    ],
];

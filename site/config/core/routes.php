<?php

return [
    /**
     * Redirects
     */
    [
        'pattern' => [
            'referendariat',
            'zweites-staatsexamen',
            'tipps-und-tricks',
        ],
        'action'  => function()
        {
            if ($page = page(kirby()->route()->pattern())) {
                return go($page->aliasPage(), 301);
            }
        },
    ],


    [
        'pattern' => [
            'landgerichte/olg-bezirk-karlsruhe/(:any)',
            'landgerichte/olg-bezirk-stuttgart/(:any)',
        ],
        'action'  => function(string $any)
        {
            if ($page = page('landgerichte/' . $any)) {
                go($page, 301);
            }
        },
    ],


    /**
     * Provide RSS feed for blog posts
     */
    [
        'pattern' => 'feed',
        'action'  => function()
        {
            # Fetch blog page
            $page = page('aktuelles');

            # Define options
            $options = [
                'link'        => $page->uid(),
                'title'       => $page->heading()->or($page->title()),
                'description' => $page->text()->kti(),
            ];

            # Create feed
            return $page->children()->listed()->flip()->limit(10)->feed($options);
        },
    ],


    /**
     * Create XML sitemap
     */
    [
        'pattern' => 'sitemap.xml',
        'action'  => function()
        {
            # Define options
            $options = [
                'images' => false,
                'videos' => false,
                'xsl'    => false,
            ];

            return site()->index()->listed()->sitemap($options);
        },
    ],
    [
        'pattern' => 'sitemap',
        'action'  => function()
        {
            return go('sitemap.xml', 301);
        },
    ],
];

<?php

return [
    # Thumbnail options go here

    # General settings
    'driver'    => 'im',
    'quality'   => 85,
    'interlace' => true,

    # Thumbnail presets
    'presets' => [
        'image' => ['width' => 640],
        'gallery' => ['width' => 360],
        'lightbox' => ['width' => 1024],
        'book' => ['width' => 240, 'quality' => 90],
    ],
];

<?php

use Exception;

use Kirby\Cms\File;
use Kirby\Cms\Page;
use Kirby\Toolkit\A;

use ConsoleTVs\Profanity\Builder;


return [
    /**
     * Handling permissions
     */

    /**
     * Restrict editing content of regional courts
     *
     * @param Kirby\Cms\Page $page
     * @throws \Exception
     * @return void
     */
    'page.*:before' => function (Page $page)
    {
        # If user is not allowed to edit page ..
        if (!$this->user()->isAllowed($page)) {
            # .. report back
            $allowed = $page->template() == 'court'
                ? sprintf('Mitgliedern LG-Bezirks "%s"', $page->title())
                : 'den Sprechervorständen'
            ;

            # .. throw exception
            throw new Exception(sprintf('Diese Aktion steht nur Admins und %s zur Verfügung!', $allowed));
        }
    },


    /**
     * Restrict editing files of regional courts
     *
     * @param Kirby\Cms\Page $page
     * @throws \Exception
     * @return void
     */
    'file.*:before' => function (File $file)
    {
        # Get current page
        $page = $file->parent();

        # If not permitted ..
        if (!$this->user()->isAllowed($page)) {
            # .. report back
            $allowed = $page->template() == 'court'
                ? sprintf('Mitgliedern LG-Bezirks "%s"', $page->title())
                : 'den Sprechervorständen'
            ;

            # .. throw exception
            throw new Exception(sprintf('Diese Aktion steht nur Admins und %s zur Verfügung!', $allowed));
        }
    },


    /**
     * Page editing
     */

    /**
     * Check for profanities
     *
     * @param Kirby\Cms\Page $newPage
     * @param Kirby\Cms\Page $oldPage
     * @return void
     */
    'page.update:after' => function (Page $newPage, Page $oldPage)
    {
        if ($this->user()->isSpeaker()) {
            # Create blocker & load dictionary
            $blocker = Builder::blocker('')->dictionary(kirby()->root('config') . '/extras/profanities.json');

            # Parse 'heading', 'text' & 'blocks' fields
            $heading = $blocker->text($newPage->heading()->toString())->badWords();
            $text = $blocker->text($newPage->text()->toString())->badWords();
            $blocks = $blocker->text(A::join($newPage->blocks()->toBlocks()->pluck('text')))->badWords();

            # Fetch profanities from both sources
            # (1) Collect words (without their 'language' specification)
            # (2) Merge collections
            # (3) Remove duplicates
            $profanities = array_unique(A::pluck(A::extend($heading, $text, $blocks), 'word'));

            # If profanities are detected ..
            if (!empty($profanities)) {
                # .. tell us about it
                $words = count($profanities) > 1 ? 'folgende Signalwörter' : 'folgendes Signalwort';

                try {
                    kirby()->email([
                        'from' => 'mailer@refbw.de',
                        'to' => 'webmaster@refbw.de',
                        'subject' => $newPage->title()->toString(),
                        'body'=> sprintf('Der Benutzer "%s" hat soeben einen Beitrag geschrieben, der %s enthält: "%s"', $this->user()->name(), $words, A::join($profanities)),
                    ]);

                } catch (Exception $e) {
                    throw new Exception(sprintf('Du hast soeben einen Beitrag geschrieben, der %s enthält: "%s". Bitte wende dich an "webmaster@refbw.de", wenn es sich um einen Fehler handeln sollte. Danke!', $words, A::join($profanities)));
                }
            }
        }
    },


    /**
     * Text processing
     */

    /**
     * Modify text being processed with `kt`
     *
     * @param string $text Processed kirbytext
     * @return string
     */
    'kirbytext:after' => function (string $text): string
    {
        # Add class to paragraph & list elements
        # See https://forum.getkirby.com/t/add-classes-to-textarea-field-output/14060/5

        $from = [];
        $from[0] = '/<p>/';
        $from[1] = '/<a href/';
        $from[2] = '/<ul>/';
        $from[3] = '/<ol>/';
        $from[4] = '/<h2>/';
        $from[5] = '/<h3>/';
        $from[6] = '/<h4>/';
        $from[7] = '/<h5>/';
        $from[8] = '/<h6>/';
        $from[8] = '/<strong>/';

        $to = [];
        $to[0] = '<p class="text">';
        $to[1] = '<a class="link" href';
        $to[2] = '<ul class="list">';
        $to[3] = '<ol class="list">';
        $to[4] = '<h2 class="bookmark">';
        $to[5] = '<h3 class="heading">';
        $to[6] = '<h4 class="heading">';
        $to[7] = '<h5 class="heading">';
        $to[8] = '<h6 class="heading">';
        $to[8] = '<strong class="font-medium">';

        return preg_replace($from, $to, $text);
    },
];

<?php

use Kirby\Cms\App;


return [
    /**
     * Includes
     */

    # Routes
    'routes' => require __DIR__ . '/core/routes.php',

    # Hooks
    'hooks' => require __DIR__ . '/core/hooks.php',

    # Thumbnails
    'thumbs' => require __DIR__ . '/core/thumbs.php',


    /**
     * Kirby settings
     */

    # Enable german slugs
    'slugs' => 'de',


    /**
     * Plugin settings
     */

    # Hook into 'ready' function
    # See https://getkirby.com/docs/reference/system/options/ready
    'ready' => function(App $kirby) {
        return [
            # Utilize manifest file with hashed assets
            'bnomei.fingerprint.query' => $kirby->root('assets') . '/manifest.json',
        ];
    },

    # Define custom path to JSON redirect file
    'distantnative.retour.config' => __DIR__ . '/extras/retour.yml',

    # Define 'focus' presets
    'flokosiol.focus.presets' => [
        'hero' => [
            'width'   => 1152,
            'height'  => 360,
            'options' => ['quality' => 75],
        ],
        'meta' => [
            'width'   => 1200,
            'height'  => 1200,
            'options' => ['quality' => 75],
        ],
        'openGraph' => [
            'width'   => 1200,
            'height'  => 630,
            'options' => ['quality' => 75],
        ],
        'twitterCard' => [
            'width'   => 1200,
            'height'  => 675,
            'options' => ['quality' => 75],
        ],
    ],

    # Enable auto-linking of legal norms
    'kirby3-gesetze.enabled' => true,

    # Increase captcha length
    'simple-captcha.length' => 6,
    'simple-captcha.width' => 240,
    'simple-captcha.height' => 48,
];

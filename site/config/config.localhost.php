<?php

return [
    # Activate debug mode
    'debug' => true,

    # Disable HTML minification
    'afbora.kirby-minify-html.enabled' => false,

    # Disable redirect logging
    'distantnative.retour.logs' => false,
];

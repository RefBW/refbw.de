Title: Ablauf

----

Heading: 

----

Text: Es ist immer gut zu wissen, was einen erwartet - hier findest du Antworten auf Fragen, die später nichts mehr nützen, vorher aber entscheidend sein können.

----

Blocks:

[
    {
        "content": {
            "level": "h2",
            "text": "Nutzung von Kommentaren"
        },
        "id": "5892a82f-dd02-45e2-aabe-faa6af72f07a",
        "isHidden": false,
        "type": "heading"
    },
    {
        "content": {
            "text": "<p>Seit dem Prüfungstermin Herbst 2012 sind auch in Baden-Württemberg Kommentare <strong>bei der Bearbeitung der Examensklausuren zugelassen</strong>. Unser Rat an dieser Stelle: Mach dich frühzeitig mit der Arbeit mit den Kommentaren vertraut. In der Klausur wird er nur helfen, wenn du ihn als Nachschlagewerk nutzen kannst. Für seitenweises Lesen im Kommentar wird keine Zeit in der Klausur sein. Der Jurist muss nicht alles wissen - er muss nur wissen, wo es steht.</p><p>Bezüglich des <strong>erforderlichen Standes</strong> der jeweils zugelassenen Kommentare für das Zweite Staatsexamen gibt das Landesjustizprüfungsamt (LJPA) vor jedem Prüfungstermin <a href=\"https://www.justiz-%20bw.de/,Lde/Startseite/Pruefungsam%20t/Zweite+juristische+Staatspruefung\" target=\"_blank\" rel=\"noopener noreferrer\">ein aktuelles Hinweisblatt</a> heraus. Vorauflagen von Kommentaren werden zwar nicht beanstandet; die <strong>Benutzung anderer als der zugelassenen Auflagen</strong> erfolgt aber auf eigene Gefahr und ist <strong>kein Remonstrationsgrund</strong>, sofern sich dadurch Fehler in der Klausurlösung einstellen.</p><p>Hierzu solltest du wissen (insbesondere dann, wenn du dein Erstes Staatsexamen nicht in Baden-Württemberg absolviert hast), dass sich die Gesetzessammlungen, anders als in anderen Bundesländern, für die <strong>Aufsichtsarbeiten</strong>, die in der <strong>ersten Jahreshälfte</strong> geschrieben werden, auf dem <strong>Stand vom Januar desselben Jahres</strong> und für die <strong>Aufsichtsarbeiten</strong>, die in der <strong>zweiten Jahreshälfte</strong> geschrieben werden, auf dem <strong>Stand vom Juli desselben Jahres</strong> befinden müssen. <strong>Für die mündliche Prüfung</strong> dagegen sollen sich die Gesetzessammlungen <strong>auf dem neuesten Stand</strong> befinden. Genauere Informationen entnimmst du bitte deiner Ladung zur Prüfung.</p>"
        },
        "id": "00dbaad8-f4cf-498e-ac33-f9f9178c568d",
        "isHidden": false,
        "type": "text"
    },
    {
        "content": {
            "level": "h3",
            "text": "Anschaffung"
        },
        "id": "1305d0d0-1aa0-4efb-8eb4-75cb2af87aba",
        "isHidden": false,
        "type": "heading"
    },
    {
        "content": {
            "text": "<p>Da die Anschaffung sämtlicher zugelassener Kommentare einen nicht ganz unerheblichen finanziellen Aufwand darstellt, solltest du dir frühzeitig - am besten schon in den ersten Wochen des Referendariats - überlegen, ob du die jeweiligen Kommentare kaufst oder mietest. Im Internet lassen sich unterschiedliche <strong>Mietangebote</strong> finden - mehr dazu findet ihr in unserer <a href=\"/tipps-und-tricks/links-und-formulare\" rel=\"noopener noreferrer\">Linksammlung</a>. Empfehlenswert ist es hierbei sicherlich, die Kommentare gruppenweise anzumieten, um Mengenrabatte zu erhalten.</p><p>Um die Kommentare solltest du dich <strong>so früh wie möglich</strong> (in der Zivilstation) kümmern, da die Koffer schnell vergriffen sind. Manchmal werden in den Bibliotheken der Landgerichte sowie bei den Staatsanwaltschaften die Vorjahresauflagen der Kommentare zu günstigen Preisen an die Referendare abgegeben; am besten informierst du dich direkt an deinem Landgericht.</p><p>Um daneben die <strong>finanzielle Belastung</strong> für die Referendare beim Erwerb der Kommentare zu verringern, wurde die Hilfsmittel-VwV dahingehend geändert, dass ab dem Prüfungstermin Herbst 2013 der <s>Schönfelder</s>Habersack- und der Sartorius-Ergänzungsband nicht mehr erforderlich sind.</p>"
        },
        "id": "233e5c2d-f7ff-4c83-8852-9a6b9737ba41",
        "isHidden": false,
        "type": "text"
    }
]

----

Uuid: vewJX6jUOWadrzGR
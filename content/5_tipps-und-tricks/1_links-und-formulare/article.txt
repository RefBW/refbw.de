Title: Links & Formulare

----

Blocks:

[
    {
        "content": {
            "text": "<p>Wir haben für dich Linktipps zu Ausbildungsstätten, Berufs- und Interessenverbänden sowie bundes- und landesrechtliche Gesetzes- und Verordnungssammlungen zusammengetragen - und vieles mehr!</p><p>Daneben findest du hier alle wichtigen Formulare, Antragsvordrucke bzw. Links zu entsprechenden Dokumenten, etwa um <a href=\"/referendariat/ueberblick#urlaub\" rel=\"noopener noreferrer\">Urlaub</a> zu beantragen oder einer Ausbildungsstätte zugewiesen zu werden. Du findest, dass etwas wichtiges fehlt? Dann <a href=\"mailto:post@refbw.de\">schreib uns</a> und wir nehmen es in die Liste auf! </p>"
        },
        "id": "a5944a61-b79a-496a-9daf-03a41bd5dad1",
        "isHidden": false,
        "type": "text"
    },
    {
        "content": {
            "level": "h2",
            "text": "Behörden &amp; Ausbildungsstätten"
        },
        "id": "57332918-21cd-4cb4-bd1e-5025016a7701",
        "isHidden": false,
        "type": "heading"
    },
    {
        "content": {
            "level": "h3",
            "text": "Ministerien &amp; Landesämter"
        },
        "id": "15c9f75b-9610-49f0-a265-189db8f58026",
        "isHidden": false,
        "type": "heading"
    },
    {
        "content": {
            "rows": [
                {
                    "name": "Landesjustizprüfungsamt",
                    "url": "https://www.justiz-bw.de/,Lde/Startseite/Pruefungsamt"
                },
                {
                    "name": "Ministerium der Justiz und für Migration",
                    "url": "https://www.justiz-bw.de"
                },
                {
                    "name": "Landesamt für Besoldung & Versorgung",
                    "url": "https://lbv.landbw.de"
                }
            ]
        },
        "id": "f242610d-defe-4762-bd36-ebb9cd997577",
        "isHidden": false,
        "type": "links"
    },
    {
        "content": {
            "level": "h3",
            "text": "Regierungspräsidien"
        },
        "id": "db331b8f-87b4-468f-9f5c-f768b497d2ac",
        "isHidden": false,
        "type": "heading"
    },
    {
        "content": {
            "rows": [
                {
                    "name": "Regierungspräsidien BW",
                    "url": "https://rp.baden-wuerttemberg.de"
                },
                {
                    "name": "Regierungspräsidium Freiburg",
                    "url": "https://rp.baden-wuerttemberg.de/rpf"
                },
                {
                    "name": "Regierungspräsidium Karlsruhe",
                    "url": "https://rp.baden-wuerttemberg.de/rpk"
                },
                {
                    "name": "Regierungspräsidium Stuttgart",
                    "url": "https://rp.baden-wuerttemberg.de/rps"
                },
                {
                    "name": "Regierungspräsidium Tübingen",
                    "url": "https://rp.baden-wuerttemberg.de/rpt"
                }
            ]
        },
        "id": "3cd0c67d-372d-47df-bcd9-aa28a38fae7f",
        "isHidden": false,
        "type": "links"
    },
    {
        "content": {
            "level": "h3",
            "text": "Sonstige Institutionen"
        },
        "id": "f16fb74e-5c6e-4387-adde-ed8e0f1d6b08",
        "isHidden": false,
        "type": "heading"
    },
    {
        "content": {
            "rows": [
                {
                    "name": "Universität für Verwaltungswissenschaften (Speyer)",
                    "url": "https://www.uni-speyer.de"
                }
            ]
        },
        "id": "4e7ecc7c-b7cd-42d5-87bb-01aba97ac2e1",
        "isHidden": false,
        "type": "links"
    },
    {
        "content": {
            "level": "h2",
            "text": "Interessenvertretungen"
        },
        "id": "8fbe2d5b-6f78-45ea-ab45-58f68940c184",
        "isHidden": false,
        "type": "heading"
    },
    {
        "content": {
            "level": "h3",
            "text": "Rechtsanwaltskammern (RAKs)"
        },
        "id": "e666b37a-5066-43e2-a4a3-d8e5a0bf25d3",
        "isHidden": false,
        "type": "heading"
    },
    {
        "content": {
            "rows": [
                {
                    "name": "Bundesrechtsanwaltskammer (BRAK)",
                    "url": "https://www.brak.de"
                },
                {
                    "name": "Rechtsanwaltskammer Freiburg",
                    "url": "https://www.rak-freiburg.de"
                },
                {
                    "name": "Rechtsanwaltskammer Karlsruhe",
                    "url": "https://www.rak-karlsruhe.de"
                },
                {
                    "name": "Rechtsanwaltskammer Stuttgart",
                    "url": "https://rak-stuttgart.de"
                },
                {
                    "name": "Rechtsanwaltskammer Tübingen",
                    "url": "https://www.raktuebingen.de"
                }
            ]
        },
        "id": "bed6f4b5-7282-4e9f-9d89-9392a4587585",
        "isHidden": false,
        "type": "links"
    },
    {
        "content": {
            "level": "h3",
            "text": "Sonstige Vereinigungen"
        },
        "id": "fb3e0fb7-96b0-4dcb-af24-81a2ca14c7ea",
        "isHidden": false,
        "type": "heading"
    },
    {
        "content": {
            "rows": [
                {
                    "name": "Deutscher Anwaltverein",
                    "url": "https://anwaltverein.de"
                },
                {
                    "name": "Deutscher Juristinnenbund",
                    "url": "https://www.djb.de"
                },
                {
                    "name": "Bundesnotarkammer",
                    "url": "https://www.bnotk.de"
                },
                {
                    "name": "Notarkammer BW",
                    "url": "https://www.notarkammer-bw.de"
                }
            ]
        },
        "id": "2f250ca6-2d6d-45c1-bb2a-3a1754db2999",
        "isHidden": false,
        "type": "links"
    },
    {
        "content": {
            "level": "h2",
            "text": "Sonstiges"
        },
        "id": "7c5a521f-aa7e-4beb-911d-9871c5c265d9",
        "isHidden": false,
        "type": "heading"
    },
    {
        "content": {
            "level": "h3",
            "text": "Klausurfälle"
        },
        "id": "16c6a4be-efc0-44b9-9afb-3f37ad66bb9e",
        "isHidden": false,
        "type": "heading"
    },
    {
        "content": {
            "rows": [
                {
                    "name": "\"Fall des Monats\" (Alpmann Schmidt)",
                    "url": "https://www.alpmann-schmidt.de/rechtsprechungsuebersicht.aspx"
                },
                {
                    "name": "Fälle zum Zivilprozessrecht (RiBGH Dr. Bacher)",
                    "url": "https://www.dr-bacher.de/AG/index.html"
                },
                {
                    "name": "\"famos\" - Fall des Monats Strafrecht (Prof. Dr. Reinbacher)",
                    "url": "http://famos.jura.uni-wuerzburg.de"
                }
            ]
        },
        "id": "84098864-7a9f-4f00-921c-a60f8c92f8eb",
        "isHidden": false,
        "type": "links"
    },
    {
        "content": {
            "level": "h3",
            "text": "Lernmaterial"
        },
        "id": "7e906a77-2524-46c3-8253-6b3a566620d9",
        "isHidden": false,
        "type": "heading"
    },
    {
        "content": {
            "rows": [
                {
                    "name": "Aktenvorträge (NRW)",
                    "url": "https://www.justiz.nrw.de/Gerichte_Behoerden/landesjustizpruefungsamt/juristischer_vorbereitungsdienst/kurzvortraege/index.php"
                },
                {
                    "name": "Ausbildungsskripte (Kammergericht Berlin)",
                    "url": "https://www.berlin.de/gerichte/kammergericht/karriere/rechtsreferendariat/vorbereitungsdienst/downloads/#ausbskr"
                }
            ]
        },
        "id": "ab03e387-fbf8-42fa-8466-a19a6750d096",
        "isHidden": false,
        "type": "links"
    },
    {
        "content": {
            "level": "h3",
            "text": "Podcasts"
        },
        "id": "c887e05d-a60d-45f0-a3e5-38da387dda64",
        "isHidden": false,
        "type": "heading"
    },
    {
        "content": {
            "rows": [
                {
                    "name": "\"AG Strafrecht\" (Christian Konert)",
                    "url": "https://ag-strafrecht.eu"
                },
                {
                    "name": "\"AG Zivilrecht\" (Christian Konert)",
                    "url": "https://www.ag-zivilrecht.de"
                },
                {
                    "name": "\"10 Minuten Jura\" (Repetitorium Hofmann)",
                    "url": "https://www.repetitorium-hofmann.de/podcast"
                }
            ]
        },
        "id": "0931d6c2-40ac-43fc-9c79-3740feae5fb8",
        "isHidden": false,
        "type": "links"
    },
    {
        "content": {
            "level": "h3",
            "text": "Verordnungen und Verwaltungsvorschriften"
        },
        "id": "163f44c6-b3fe-4dd1-9a09-9c9161ed68e2",
        "isHidden": false,
        "type": "heading"
    },
    {
        "content": {
            "rows": [
                {
                    "name": "Bundesrecht",
                    "url": "https://www.gesetze-im-internet.de"
                },
                {
                    "name": "Landesrecht BW",
                    "url": "https://www.landesrecht-bw.de"
                },
                {
                    "name": "Ausbildungs- & Prüfungsrecht BW",
                    "url": "https://rechtsreferendariat-bw.justiz-bw.de/pb/,Lde/Startseite/Ausbildungs-+und+Pruefungsrecht/Verordnungen+und+Verwaltungsvorschriften"
                }
            ]
        },
        "id": "9ae2d5e3-0f3f-405a-a3e6-8c15c85eff43",
        "isHidden": false,
        "type": "links"
    }
]

----

Heading: 

----

Text: In der nachfolgenden Übersicht findest du viele Institutionen, die dich während des Referendariats - und möglicherweise darüber hinaus - begleiten werden.

----

References: 

----

Downloads:

- 
  name: Welcome-Paper (Karlsruhe)
  file:
    - welcome-paper-2022.pdf
- 
  name: Welcome-Paper (Stuttgart)
  file:
    - welcome-paper-2021.pdf

----

Uuid: uz7F5wWsZZXrNSIN
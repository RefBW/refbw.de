Title: Universität Hohenheim

----

Logo: - file://mdEm02e6ySGdaUEM

----

Text: Die Universität Hohenheim ist eine Campus-Universität im Stuttgarter Stadtbezirk Plieningen. Große Teile der Universität sind im Schloss Hohenheim untergebracht. Ihre fachlichen Schwerpunkte sind Agrar-, Natur-, Kommunikations- und Wirtschaftswissenschaften.

----

Homepage: https://www.uni-hohenheim.de

----

Email:

----

Uuid: gb6IwCh6Sy3oyY8F

----

Blocks:

----

References:

----

Downloads:

Title: Referendariat bei Caemmerer Lenz in Karlsruhe

----

Offerer: stellenangebote/anbieter/caemmerer-lenz

----

Sticky: false

----

Text: Als Referendar:in hast Du die Grundlagen Deiner zukünftigen Tätigkeit bereits erlernt. Nun brauchst Du praktische Erfahrung. Unsere Sozietät ist eine der ersten Adressen im Anwaltsmarkt im Südwesten Deutschlands. Die große Anzahl an Beratern und die große Zahl der Rechtsgebiete, die diese abdecken, öffnen Dir das Tor zu vielen beruflichen Erfahrungen. Wir unterstützen Dich auch und besonders dabei, eigenständig und praxisorientiert zu arbeiten.

----

Blocks:

[
    {
        "content": {
            "level": "h2",
            "text": "Deshalb bilden wir aus"
        },
        "id": "832bf522-da94-48ee-9b55-5209dd086cff",
        "isHidden": false,
        "type": "heading"
    },
    {
        "content": {
            "text": "<p>Wir wissen, dass der Anwaltsberuf erlernt werden muss. Wir wissen auch, dass die Ausbildung an den Universitäten und im Referendariat vieles von dem, was in der Praxis benötigt wird, nicht abdeckt.</p><p><strong>Hand aufs Herz:</strong> Wissen Sie, wie man mit schwierigen Mandanten umgeht, wie Pricing funktioniert und wie anwaltliche Produktgestaltung geht – zumal in Zeiten der digitalen Transformation? All dies gehört zu unserem Beruf, genauso wie das richtige Formulieren von Anträgen und das überzeugende Argumentieren.</p><p>Damit Sie nach dem zweiten Staatsexamen vorbereitet sind, und weil wir der Meinung sind, dass es Aufgabe des Referendariats ist, auf die Berufsausübung vorzubereiten, haben wir bereits vor vielen Jahren einen Entschluss gefasst:</p><p><strong>Wir wollen Referendare nicht beschäftigen, sondern ausbilden, und zwar in allen Stationen. Das gilt in besonderem Maße für die Wahlstation, in der der Blick in der Regel weiter in die Zukunft gerichtet ist als zuvor.</strong></p>"
        },
        "id": "e599ac09-0752-409d-9134-abd6babd9861",
        "isHidden": false,
        "type": "text"
    },
    {
        "content": {
            "level": "h2",
            "text": "Wie wir ausbilden"
        },
        "id": "f76d2c3a-3ded-4e3b-9d27-e9853f549dce",
        "isHidden": false,
        "type": "heading"
    },
    {
        "content": {
            "text": "<p>Sie werden gleich zu Beginn an die Praxis herangeführt, indem Sie an einer Einführungsveranstaltung teilnehmen. Dort wird das, was Sie beispielsweise zur Büroorganisation wissen möchten und auch wissen müssen, besprochen.</p><p>Sie lernen dann bei Ihrem Rechtsanwalt bzw. bei Ihrer Rechtsanwältin die<strong> ganze Bandbreite der anwaltlichen Tätigkeit</strong> kennen. Dabei ist Ihre Tätigkeit nicht auf Zuarbeiten beschränkt. Wir ermöglichen Ihnen von Beginn an unter Anleitung die <strong>eigenständige Arbeit</strong> am Fall und mit den Mandanten.</p><p>Sie profitieren bei uns – je nach Station und Schwerpunkt – von einer <strong>examensrelevanten und praxisorientierten Ausbildung</strong>. Dabei können Sie in dem gewünschten Rechtsgebiet tätig sein, wobei Ihnen ein fester Ansprechpartner zur Verfügung steht. Sie haben aber selbstverständlich auch die Möglichkeit, über Ihr Fachgebiet hinaus andere Referate kennen zu lernen. Sie haben nicht nur die Möglichkeit, an Besprechungs- und Verhandlungsterminen teilzunehmen. Sie können auch Kanzleiveranstaltungen kennenlernen. Auch finden immer wieder Workshops statt, in denen gezielt das erörtert wird, was für Sie als Referendare wichtig ist.</p><p>Einen starren Zeitplan gibt es bei uns nicht. Es bestehen <strong>flexible Arbeitszeiten</strong>, die Ihnen Raum für sonstige Verpflichtungen oder Freizeit lassen.</p>"
        },
        "id": "68dcc1f8-0e57-471e-9a87-486be830bca7",
        "isHidden": false,
        "type": "text"
    },
    {
        "content": {
            "level": "h3",
            "text": "Kontakt"
        },
        "id": "633aa3e8-c9d6-484b-83ef-ab3a79d52fb9",
        "isHidden": false,
        "type": "heading"
    },
    {
        "content": {
            "text": "<p>Für eine Einstellung zum nächstmöglichen Zeitpunkt freuen wir uns über Ihre Bewerbungsunterlagen, bevorzugt per Mail oder postalisch.</p>"
        },
        "id": "eacffd4d-2c9d-4180-9b3b-c11ab8f127c9",
        "isHidden": false,
        "type": "text"
    }
]

----

Tags: Öffentliches Recht, Wirtschaftsrecht

----

Locations: Karlsruhe

----

Period: Stationsausbildung

----

Misc:

----

Date: 2022-10-06

----

Ends: 2022-10-06

----

Homepage: https://www.caemmerer-lenz.de/karriere-und-ausbildung/karriere-und-ausbildung-fuer-referendare

----

Email:

----

Social:

----

Downloads:

-
  name: Flyer
  file:
    - file://WRSE0zqeOmZ4znZs

----

Uuid: i9CnQpqBrJiCwuOS

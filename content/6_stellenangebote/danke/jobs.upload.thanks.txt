Title: Danke!

----

Blocks:

[
    {
        "content": {
            "level": "h2",
            "text": "Die nächsten Schritte"
        },
        "id": "60d635c2-b53f-4821-8d88-5e45b2639f78",
        "isHidden": false,
        "type": "heading"
    },
    {
        "content": {
            "text": "<p>Nachdem du das Formular abgeschickt hast, prüfen wir die eingegangenen Daten zunächst auf Vollständigkeit; sollten wichtige Angaben fehlen, melden wir uns per Email oder telefonisch bei dir.</p><p>Bis dahin heißt es ..</p>"
        },
        "id": "82a4c933-1ab8-47c0-8605-300af02eba94",
        "isHidden": false,
        "type": "text"
    },
    {
        "content": {
            "image": [
                "kimdonkey@unsplash.jpg"
            ],
            "alt": "",
            "caption": "",
            "link": ""
        },
        "id": "36fa2d83-3578-4524-adf2-991ab94e3a42",
        "isHidden": false,
        "type": "image"
    },
    {
        "content": {
            "text": "<p>Die redaktionelle Überarbeitung von Stellenangeboten geht dann sehr schnell - in der Regel gehen vollständig eingereichte Anzeigen innerhalb weniger Stunden nach ihrem Eingang bei uns online! Gleiches gilt für Änderungswünsche eines bereits bestehenden Stellenangebots.</p>"
        },
        "id": "6af4e106-9591-4867-a5b7-cbda82abd3a7",
        "isHidden": false,
        "type": "text"
    }
]

----

Heading: Danke für deine Nachricht!

----

Text: Wir haben deine Nachricht erhalten und kümmern uns schnellstmöglich um dein Stellenangebot. Hier erfährst du, wie es nun weitergeht.

----

References: 

----

Downloads: 

----

Offerer: px9owkye

----

Tags: 

----

Sticky: true

----

Date: 2022-03-20

----

Ends: 

----

Homepage: https://refbw.de

----

Email: post@refbw.de

----

Social:

- 
  name: facebook
  url: >
    https://www.facebook.com/SprechervorstandOlgKarlsruhe

----

Logo: - refbw.png

----

Author: - 0hzY98ZV

----

Uuid: txnENdx3rpyPUxgb
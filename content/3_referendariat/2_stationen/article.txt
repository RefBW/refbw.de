Title: Stationen

----

Blocks:

[
    {
        "content": {
            "level": "h2",
            "text": "Das kommt auf dich zu"
        },
        "id": "4b970c5a-5b76-4dcc-9e98-707ec16cc17d",
        "isHidden": false,
        "type": "heading"
    },
    {
        "content": {
            "text": "<p>Das Referendariat in Baden-Württemberg ist in folgende Stationen gegliedert:</p>"
        },
        "id": "016ceeea-e0fc-46df-9fc6-377728368883",
        "isHidden": false,
        "type": "text"
    },
    {
        "content": {
            "text": "<ul><li>Zivilstation (5 Monate)</li><li>Strafstation (3,5 Monate)</li><li>RA-Station I (4,5 Monate)</li><li>Verwaltungsstation (3,5 Monate)</li><li>RA-Station II (4,5 Monate)</li><li>Wahlstation (3 Monate)</li></ul>"
        },
        "id": "5f03fc4c-75b6-4e4e-a354-00d89ad54ed3",
        "isHidden": false,
        "type": "list"
    },
    {
        "content": {
            "level": "h3",
            "text": "Infos zum Ablauf"
        },
        "id": "e49ed8f3-e45b-4fa4-9730-b971ff417dfd",
        "isHidden": false,
        "type": "heading"
    },
    {
        "content": {
            "text": "<p>Zu Beginn der ersten vier Stationen findet ein <strong>Einführungslehrgang</strong> statt, bei dem du mit den wichtigsten Informationen und Kenntnissen vertraut gemacht wirst. Während jeder Station hast du dann im Schnitt einmal wöchentlich eine sogenannte <strong>AG-Stunde</strong>. Bei diesen Lehrveranstaltungen wirst du von wechselnden Dozenten mit den unterschiedlichsten Rechtsgebieten vertraut gemacht - wie eine Vorlesung an der Uni, aber mit starkem Praxisbezug und in der Regel wesentlich interaktiver.</p><p>Am Ende der RA-Station II findet <strong>der schriftliche Teil</strong> der Zweiten juristischen Staatsprüfung statt, <strong>der mündliche Teil</strong> folgt dann nach der Wahlstation. Die Anmeldung zum <a href=\"/zweites-staatsexamen/vorbereitung\" rel=\"noopener noreferrer\">Staatsexamen</a> erfolgt etwa ein halbes Jahr vor den schriftlichen Prüfungen. Es besteht die Möglichkeit, den schriftlichen Teil an einem anderen Landgericht zu schreiben - dies muss allerdings ausreichend begründet werden. Die mündliche Prüfung findet dagegen ausnahmslos in Stuttgart statt. Das Referendariat wird mit einer feierlichen Examensfeier, ebenfalls in Stuttgart, abgeschlossen.</p><p>Was die <strong>Bewerbung auf die Ausbildungsstellen</strong> in den verschiedenen Stationen angeht, so wirst du von deinem Ausbildungsleiter rechtzeitig mit den notwendigen Informationen versorgt. Für die Zivil-, Straf- und Verwaltungsstation erfolgt die <strong>Zuteilung über den Ausbildungsleiter bzw. das Regierungspräsidium</strong>.</p><p>Eine <strong>Ausbildungsstelle für die Rechtsanwaltsstationen</strong> und die Wahlstation musst du dir rechtzeitig <strong>selbst suchen</strong>. Die Zuweisungsgesuche werden rechtzeitig ausgeteilt bzw. sind auf der <a href=\"/landgerichte\" rel=\"noopener noreferrer\">Webseite deines OLGs</a> abrufbar und müssen am Landgericht <strong>fristgerecht</strong> abgegeben werden.</p>"
        },
        "id": "f3c40680-e8d2-44b1-b5e6-fda998e3394c",
        "isHidden": false,
        "type": "text"
    },
    {
        "content": {
            "level": "h2",
            "text": "Zivilstation"
        },
        "id": "7220ff3d-7121-4b52-88da-c54f9c8dcb3c",
        "isHidden": false,
        "type": "heading"
    },
    {
        "content": {
            "text": "<p>In der Zivilstation bist du einem <strong>Richter am Amtsgericht oder Landgericht</strong> zugeteilt, für den du Gutachten verfasst, Entscheidungsentwürfe schreibst oder auch - das gestattet § 10 GVG - mindestens eine mündliche Verhandlung selbst leitest.</p><p>Es empfiehlt sich, den Stationsausbilder bereits möglichst früh darauf anzusprechen, da der Ausbildungsrichter erst einen geeigneten Fall für \"deine\" Verhandlung finden muss. Außerdem hast du so die Möglichkeit, dich schon frühzeitig mit \"deiner\" Akte vertraut zu machen. Auch an dieser Stelle der Tipp: einfach trauen! Der Ausbildungsrichter sitzt neben einem und kann zur größten Not eingreifen.</p><p>Es ist zwar weit verbreitet, bleibt aber ein Vorurteil, dass sich die Referendarstätigkeit am Landgericht von der am Amtsgericht unterscheidet. Das stimmt nur, soweit es um die unterschiedlichen sachlichen Zuständigkeiten geht; in puncto Arbeitsstil und Arbeitsumfang richtet sich dies vor allem nach der individuellen Arbeitsweise des jeweiligen ausbildenden Richters. Hierüber kannst du dich bei den älteren Referendar:innen informieren. Landgerichtsakten sind zwar grundsätzlich etwas umfangreicher, dafür muss man dort im Schnitt weniger Akten für einen Termin vorbereiten als beim Amtsgericht.</p>"
        },
        "id": "e38c8fbd-023d-4b56-ac83-31d25d79cbf4",
        "isHidden": false,
        "type": "text"
    },
    {
        "content": {
            "level": "h2",
            "text": "Strafstation"
        },
        "id": "690b0bc6-143b-4d3e-b1e1-28c004a9c12f",
        "isHidden": false,
        "type": "heading"
    },
    {
        "content": {
            "text": "<p>In der Strafstation wirst du <strong>der Staatsanwaltschaft oder einem Amts- oder Landgericht</strong> zugeteilt. Dafür werden gegen Ende der Zivilstation Wahlbögen ausgegeben, auf denen du deine Zuteilungswünsche zu einem bestimmten Staatsanwalt oder zu einer bestimmten Abteilung der Staatsanwaltschaft angeben kannst; für die Zuteilung an einen Richter gilt dasselbe.</p><p>Bei der Staatsanwaltschaft wirst du vor allem <strong>staatsanwaltschaftliche Abschlussverfügungen</strong> schreiben, bei Gericht an den Sitzungen deines Richters teilnehmen und die anfallenden <strong>Urteile und Beschlüsse</strong> vorbereiten - im Staatsexamen wird beides verlangt. Wegen des \"hohen Ausbildungswertes des Sitzungsdienstes\" soll die Zuweisung allerdings - im Rahmen der vorhandenen Kapazitäten - vorrangig an die Staatsanwaltschaft erfolgen, sodass in der Regel alle Referendar:innen Sitzungsdienst leisten.</p><p>Egal, wie du dich letztlich entscheidest, wirst du (mehrmals) während der Station den <strong>staatsanwaltschaftlichen Sitzungsdienst vor dem Strafrichter</strong> an einem Amtsgericht deines Landgerichtsbezirks übernehmen. Dabei bist du selbständig als Sitzungsvertreter der Staatsanwaltschaft tätig - lediglich Verfahrenseinstellungen und der Verzicht auf Rechtsmittel sind Referendar:innen (ohne vorherige Absprache) verboten. Wirst du zur Sitzungsvertretung an einem auswärtigen Amtsgericht eingeteilt, bekommst du die <a href=\"/referendariat/mobilitaet\" rel=\"noopener noreferrer\">Fahrtkosten</a> auf Antrag erstattet - Informationen hierzu erhältst du von der Ausbildungsleitung kurz vor Beginn der Strafstation.</p><p>Zur Vorbereitung auf den Sitzungsdienst findet nach dem Einführungslehrgang ein <strong>Plädierkurs unter der Leitung eines Staatsanwalts</strong> statt, der einem auch beim ersten Plädoyer am Gericht zur Seite steht. Aufgrund der Verkürzung des Referendariats ist die Staatsanwaltschaft in den Monaten Mitte Juni bis August und Mitte Dezember bis Februar ohne Referendare. Es besteht deshalb die Möglichkeit, in dieser Zeit – d.h. über die Strafstation hinaus – <strong>Sitzungsdienst als Nebentätigkeit</strong> für die Staatsanwaltschaft wahrzunehmen. Der Sitzungsdienst ist als Nebentätigkeit generell genehmigt und wird mit <strong>15,00 Euro pro Stunde</strong> vergütet (Stand: August 2021).</p>"
        },
        "id": "4b83af4d-f480-44c5-9944-db4c49590ed5",
        "isHidden": false,
        "type": "text"
    },
    {
        "content": {
            "text": "<p><strong>Praxistipp:</strong> Männer sollten sich rechtzeitig eine <strong>weiße Krawatte besorgen</strong>. Spätestens im Plädierkurs wird man(n) sie brauchen. Sehr günstig bekommt man einfache weiße Seidenkrawatten im Bastelzubehör (zwischen fünf bis sieben Euro), ansonsten natürlich auch in den örtlichen Bekleidungsgeschäften. Frauen brauchen eine weiße Bluse mit ordentlichem Kragen bzw. einen weißen Schal.</p>"
        },
        "id": "43fe9e03-18d1-4cf1-9868-beb87239e7f0",
        "isHidden": false,
        "type": "note"
    },
    {
        "content": {
            "level": "h2",
            "text": "Anwaltsstationen I &amp; II"
        },
        "id": "90e66c94-85f8-4f10-8f93-43a24d6c2e0e",
        "isHidden": false,
        "type": "heading"
    },
    {
        "content": {
            "text": "<p>Die Anwaltsstationen werden, ebenso wie die Wahlstation, vom OLG geleitet. Hier musst du dich <strong>selbstständig</strong> um eine Ausbildungsstelle bei einem Rechtsanwalt kümmern - eine Pflicht der Rechtsanwälte zur Ausbildung von Referendar:innen besteht indes nicht.</p><p>Du bist bei deiner Wahl nicht auf die örtlichen Kanzleien beschränkt. Es gibt auch die Möglichkeit, die Station bei einem Rechtsanwalt außerhalb des LG-Bezirks abzuleisten; gegebenenfalls musst du dich dafür <strong>vom AG-Unterricht befreien</strong> lassen; hierfür ist die <strong>Zustimmung des Ausbildungsleiters erforderlich</strong>. Zu beachten ist hierbei jedoch, dass die Befreiung für beide Rechtsanwaltsstationen vom AG-Unterricht grundsätzlich nicht möglich ist. Hilfreich sind die Websites der örtlichen Anwaltskammern sowie die Website der Bundesanwaltskammer, die Ausbildungsangebote, aber auch Jobangebote für Referendar:innen veröffentlichen.</p><p>Die Rechtsanwaltsstation beginnt zunächst mit einem <strong>Einführungslehrgang</strong>, welcher die verschiedenen Rechtsgebiete aus Anwaltssicht darstellt und von den Rechtsanwaltskammern an verschiedenen Orten gruppenweise durchgeführt wird. Er sollte angesichts der <strong>immer stärkeren Anwaltsorientierung des Referendariats</strong> und der Möglichkeit, im Rahmen von Examensklausuren \"in angemessenem Umfang Rechtsgestaltung und Rechtsberatung“ abzuprüfen (vgl. § 55 Abs. 3 a.E. JAPrO), nicht unterbewertet werden.</p><p>Die Anwaltsstationen sind gewöhnlich sehr arbeitsreich, was auch am dicht gefüllten Lehrplan liegt. Die meisten Anwälte sind aber gern bereit, dir entgegenzukommen und richten sich nach deinen Vorstellungen und Terminen, sodass du dir die <strong>Anwaltsstationen in großem Maße selbst gestalten kannst</strong> - vorausgesetzt, du zeigst ein entsprechendes Engagement. Die meisten Anwälte behandeln dich nach einiger Zeit wie einen echten Kollegen - wenn du Glück hast, macht dir dein Anwalt sogar ein Jobangebot!</p><p>In den Anwaltsstationen besteht daneben häufig die Möglichkeit, sich etwas dazuzuverdienen. Mancher Anwalt gewährt seinem Referendar ein zusätzliches Stationsentgelt, das unter Umständen anrechnungsfrei neben der <a href=\"/referendariat/ueberblick\" rel=\"noopener noreferrer\">Unterhaltsbeihilfe</a> bezogen werden kann und unter deine normale Steuerklasse (meist Steuerklasse 1) fällt - und nicht unter die stärker besteuerte Steuerklasse 6.</p>"
        },
        "id": "76c642ac-04f4-4511-bbbb-2a5379ed6a00",
        "isHidden": false,
        "type": "text"
    },
    {
        "content": {
            "text": "<p><strong>Praxistipp:</strong> Die Rechtsanwaltskammern (RAKs) bieten <a href=\"/tipps-und-tricks/links-und-formulare#rechtsanwaltskammern-raks\" rel=\"noopener noreferrer\">auf ihren Homepages</a> regelmäßig Stellenangebote bzw. eine Auflistung aller ausbildenden Kanzleien an. Zur Bewerbung reicht es normalerweise aus, eine kurze E-Mail mit einem aktuellen Lebenslauf zu schicken. Am einfachsten ist es, wenn du bei der Kanzlei deiner Wahl kurz anrufst und nachfragst, wie dort Bewerbungen gehandhabt werden. Eine Zuweisung zu einem Syndikusanwalt in einer Rechtsabteilung eines Unternehmens oder Verbands ist nur in der RA-Station II möglich!</p>"
        },
        "id": "e48c8134-2fb6-4de3-b070-a1e3b8e02a64",
        "isHidden": false,
        "type": "note"
    },
    {
        "content": {
            "text": "<p>Den Lehrplan der Lehrveranstaltungen, welche die Rechtsanwaltskammern durchführen, kannst du auf der jeweiligen Homepage einsehen. Auf den Internetseiten der RAKs erhälst du weitere Informationen zum Referendariat aus Sicht der Anwaltschaft.</p>"
        },
        "id": "fc31da1e-5ad0-4125-99d1-48ab03d86e8f",
        "isHidden": false,
        "type": "text"
    },
    {
        "content": {
            "level": "h2",
            "text": "Verwaltungsstation"
        },
        "id": "4352ab3f-a0e3-457d-aa3e-d49b26304061",
        "isHidden": false,
        "type": "heading"
    },
    {
        "content": {
            "text": "<p>Für die Verwaltungsstation übernehmen die Regierungspräsidien die Verteilung der Ausbildungsstellen: das <strong>Regierungspräsidium Karlsruhe</strong> für die LG-Bezirke Baden-Baden, Heidelberg, Karlsruhe, Mannheim und Mosbach, das <strong>Regierungspräsidium Freiburg</strong> für die LG-Bezirke Freiburg, Konstanz, Offenburg und Waldshut-Tiengen.</p><p>Als mögliche Ausbildungsstellen kommen nach <a href=\"https://www.landesrecht-bw.de/jportal/?quelle=jlink&amp;query=JAPV+BW+%C2%A7+47&amp;psml=bsbawueprod.psml&amp;max=true\" target=\"_blank\" rel=\"noopener noreferrer\">§ 47 Abs. 1 Nr. 4 JAPrO</a></p>"
        },
        "id": "959dcad7-b1bf-4dc2-855b-5a53cd2addc0",
        "isHidden": false,
        "type": "text"
    },
    {
        "content": {
            "text": "<ul><li>ein <strong>Landratsamt</strong>, eine <strong>Stadt</strong>, eine <strong>Gemeinde </strong>oder eine<strong> Verwaltungsgemeinschaft</strong>,</li><li>ein <strong>Regierungspräsidium</strong>, eine <strong>Landesoberbehörde</strong>, ein <strong>Landesministerium</strong>,</li><li>die <strong>Landtagsverwaltung</strong>, eine <strong>Landtagsfraktion</strong>,</li><li>eine <strong>höhere Sonderbehörde</strong>, der <strong>Landesbeauftragte für den Datenschutz</strong> und die Informationsfreiheit,</li><li>eine <strong>Polizeidienststelle</strong>,</li><li>die <strong>Oberfinanzdirektion</strong>, ein <strong>Finanzamt</strong>,</li><li>ein <strong>kommunaler Landesverband</strong>, ein <strong>Regionalverband</strong>,</li><li>die <strong>Landesanstalt für Kommunikation</strong>, eine <strong>Landesrundfunkanstalt</strong>,</li><li>eine <strong>Hochschulverwaltung</strong>,</li><li>eine <strong>Industrie- und Handelskammer</strong>, eine <strong>Handwerkskammer</strong>,</li><li>ein <strong>Verwaltungsgericht</strong>, der <strong>Verwaltungsgerichtshof</strong>,</li><li>ein <strong>Sozialgericht</strong>, das <strong>Landessozialgericht</strong>, das <strong>Finanzgericht</strong>,</li><li>eine <strong>Rechtsanwaltskammer</strong>, die <strong>Notarkammer</strong>,</li><li>die <strong>Deutsche Universität für Verwaltungswissenschaften Speyer</strong>,</li><li>die <strong>Europäische Union</strong>, der <strong>Europarat</strong></li></ul>"
        },
        "id": "6dda384f-1f69-46cf-9738-920f0010e885",
        "isHidden": false,
        "type": "list"
    },
    {
        "content": {
            "text": "<p>in Betracht.</p><p>Du bekommst im Vorfeld von dem zuständigen Regierungspräsidium eine <strong>Liste mit den möglichen Ausbildungsstellen</strong> und hast die Möglichkeit, <strong>Zuteilungswünsche</strong> anzugeben. Du kannst dich auch <strong>selbst um einen Ausbildungsplatz bemühen</strong>, indem du dich mit dem Leiter der gewünschten Stelle in Verbindung setzt; darum musst du dich allerdings selbst rechtzeitig kümmern! Inhaltliche Anforderungen sind hierbei die Gewährleistung einer sachgerechten juristischen Ausbildung, sowie das Vorliegen einer Einverständniserklärung der jeweiligen Ausbildungsstelle. Liegt die Ausbildungsstelle im Bezirk eines anderen Regierungspräsidiums, brauchst du die Zustimmung dieses und deines Regierungspräsidiums. Dein Landgericht wird dann deine Zuweisungswünsche sammeln und an das zuständige Regierungspräsidium weiterleiten. Die endgültige Zuweisung erfolgt dann direkt über das Regierungspräsidium.</p><p>Eine <strong>interessante Möglichkeit</strong> für die Verwaltungsstation bietet das sogenannte \"<strong>Speyer-Semester</strong>\". Jeder, der sich erfolgreich für einen Platz beworben hat, wird für dreieinhalb Monate der Deutschen Universität für Verwaltungswissenschaften in Speyer zugewiesen. Diese \"Zusatzausbildung\" bietet sich insbesondere dann an, wenn eine spätere Arbeit in der Verwaltung angestrebt wird. Die Plätze für Speyer werden ausschließlich nach dem Ergebnis des ersten Staatsexamens vergeben und richten sich nach der Anzahl der Bewerbungen. Baden-Württemberg verfügt über eine bestimmte Quote an Plätzen für Speyer und hat diese Quote intern auf die vier Regierungsbezirke aufgeteilt.</p>"
        },
        "id": "a3af5e33-6622-42a0-8f75-d4af1436cf01",
        "isHidden": false,
        "type": "text"
    },
    {
        "content": {
            "level": "h2",
            "text": "Wahlstation &amp; Schwerpunktbereich"
        },
        "id": "d782c68d-91ca-4e2e-9171-e8779fe65ff0",
        "isHidden": false,
        "type": "heading"
    },
    {
        "content": {
            "text": "<p>Für die Wahlstation, die <strong>nach Ableistung des schriftlichen Teils</strong> der Zweiten Juristischen Staatsprüfung beginnt, findest du eine nicht abschließende Aufzählung in <a href=\"https://www.landesrecht-bw.de/jportal/?quelle=jlink&amp;query=JAPV+BW+%C2%A7+47&amp;psml=bsbawueprod.psml&amp;max=true\" target=\"_blank\" rel=\"noopener noreferrer\">§ 47 Abs. 1 Nr. 5 JAPrO</a>. Darüber hinaus gibt es an jedem Landgericht Ordner, die eine Übersicht und Angebote über mögliche Ausbildungsstellen für die Wahlstation enthalten. Es ist auch möglich, die Wahlstation <strong>im Ausland</strong> zu verbringen. Darum musst du dich ebenfalls <strong>rechtzeitig</strong> kümmern, um nicht kurz vor den schriftlichen Prüfungen noch wegen der Unterkunft oder dem Visum in Zeitdruck zu geraten. Eine Teilung der Wahlstation ist nicht mehr möglich. Eine Ausnahme wird nur gemacht, wenn die Wahlstation an einer Universität abgeleistet wird. Ein spezielles \"Merkblatt für die Ausbildung in der Wahlstation\" erhältst du auf der Homepage <a href=\"/landgerichte\" rel=\"noopener noreferrer\">deines Oberlandesgerichts</a>. Die Wahlstation und der Schwerpunktbereich für die Zweite juristische Staatsprüfung sind mittlerweile entkoppelt.</p><p>Die Erklärung über die <strong>Wahl deines Schwerpunktes </strong>ist<strong> mit der Anmeldung zum Zweiten Staatsexamen</strong> abzugeben, also ungefähr ein halbes Jahr, bevor du dein Examen schreibst. Beachte aber, dass die Wahl nach <a href=\"https://www.landesrecht-bw.de/jportal/?quelle=jlink&amp;query=JAPV+BW+%C2%A7+54&amp;psml=bsbawueprod.psml&amp;max=true\" target=\"_blank\" rel=\"noopener noreferrer\">§ 54 Abs. 1 Nr. 4 aE JAPrO</a> <strong>unwiderruflich</strong> ist!</p><p>Welche Schwerpunkte zur Auswahl stehen und was dabei behandelt wird, ergibt sich aus der <a href=\"https://www.landesrecht-bw.de/jportal/?quelle=jlink&amp;query=VVBW-JM-20200301-SF&amp;psml=bsbawueprod.psml&amp;max=true&amp;aiz=true\" target=\"_blank\" rel=\"noopener noreferrer\">Verwaltungsvorschrift des Justizministeriums</a> über die Stoffpläne für die Lehrveranstaltungen in den Arbeitsgemeinschaften des juristischen Vorbereitungsdienstes.</p>"
        },
        "id": "e78af77d-8dbc-4912-b556-9c8278fac85a",
        "isHidden": false,
        "type": "text"
    },
    {
        "content": {
            "text": "<p><strong>Hintergrund:</strong> Früher wurde mit der Wahl der Ausbildungsstelle gleichzeitig der Schwerpunktbereich für das Zweite Staatsexamen festgelegt (\"Konnexität\" zwischen Wahlstation und Schwerpunktbereich). Im Herbst 2015 wurde diese Konnexität von den Ausbildungsverantwortlichen für die OLG-Bezirke Karlsruhe und Stuttgart aufgehoben - mit der Folge, dass du durch die Wahl der Ausbildungsstelle ab sofort nicht mehr zur Wahl eines bestimmten Schwerpunktbereichs gezwungen wirst. Unabhängig von Deiner Wahlstation kannst Du also frei Deinen Schwerpunktbereich für den mündlichen Teil des Zweiten Staatsexamens wählen.</p>"
        },
        "id": "dffc2cc8-42ad-49cc-b039-58cc016bd370",
        "isHidden": false,
        "type": "note"
    },
    {
        "content": {
            "level": "h2",
            "text": "Sonstige Angebote"
        },
        "id": "f12ed573-7dbf-48ad-bbbe-0fe5e1c6e00d",
        "isHidden": false,
        "type": "heading"
    },
    {
        "content": {
            "text": "<p>Während der Zivilstation hast du die Möglichkeit, an einem oder an zwei Tagen die Arbeit eines <strong>Gerichtsvollziehers</strong> kennenzulernen. In der Strafstation kannst du einen Einblick in die Tätigkeit der <strong>Kriminalpolizei</strong> erhalten und eventuell auch an einer Durchsuchung teilnehmen. Hier ist in den meisten Fällen Eigeninitiative gefragt. Du solltest dich daher <strong>rechtzeitig</strong> bei der Staatsanwaltschaft oder deinem Ausbildungsleiter <strong>informieren</strong>.</p><p>In Zeiten von <a href=\"/referendariat/corona\" rel=\"noopener noreferrer\">Corona</a> kann es jedoch sein, dass diese Möglichkeiten nicht oder nur in beschränktem Umfang bestehen. Es geben sich aber alle die größtmögliche Mühe, um die Aktivitäten zu ermöglichen.</p>"
        },
        "id": "5bc76bcf-ee17-47fa-aaf0-8aad4ab10c63",
        "isHidden": false,
        "type": "text"
    }
]

----

Heading: 

----

Text: Im Laufe des Referendariats durchläufst du mehrere Stationen - hier erfährst du, was dich in den einzelnen Abschnitten erwartet.

----

References: 

----

Downloads: 

----

Uuid: 0TzD3zC18hJgzgZp
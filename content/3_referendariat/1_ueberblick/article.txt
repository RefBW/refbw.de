Title: Überblick

----

Blocks:

[
    {
        "content": {
            "level": "h2",
            "text": "Allgemeines"
        },
        "id": "d6c87f23-ced9-47e5-8f5a-a19d1c4c347a",
        "isHidden": false,
        "type": "heading"
    },
    {
        "content": {
            "text": "<p>Das Referendariat ist als <strong>öffentlichrechtliches Ausbildungsverhältnis</strong> ausgestaltet und umfasst in Baden-Württemberg <a href=\"/referendariat/stationen\" rel=\"noopener noreferrer\">folgende Stationen</a>:</p>"
        },
        "id": "9cc9ee76-bed9-4487-b985-0da429537a60",
        "isHidden": false,
        "type": "text"
    },
    {
        "content": {
            "text": "<ul><li><p>Zivilstation (5 Monate)</p></li><li><p>Strafstation (3,5 Monate)</p></li><li><p>RA-Station I (4,5 Monate)</p></li><li><p>Verwaltungsstation (3,5 Monate)</p></li><li><p>RA-Station II (4,5 Monate)</p></li><li><p>Wahlstation (3 Monate)</p></li></ul>"
        },
        "id": "4549a8d7-985a-408b-b29a-ba3a9f586492",
        "isHidden": false,
        "type": "list"
    },
    {
        "content": {
            "level": "h3",
            "text": "Besoldung und Beihilfen"
        },
        "id": "0bd1e9ac-2c61-440d-aa24-992afb194827",
        "isHidden": false,
        "type": "heading"
    },
    {
        "content": {
            "text": "<p>Für ihre Tätigkeit erhalten Rechtsreferendar:innen nach § 1 Abs. 1 der Verordnung des Finanzministeriums über die Gewährung von Unterhaltsbeihilfen an Rechtsreferendare eine <strong>monatliche Unterhaltsbeihilfe</strong> in Höhe von derzeit <strong>1.352,51 Euro brutto</strong> (Stand: August 2021). Diese wird am letzten Werktag jedes Monats für den laufenden Monat gezahlt. Die Unterhaltsbeihilfe erhöht sich zur gleichen Zeit und mit dem gleichen Prozentsatz, mit dem sich der höchste Anwärtergrundbetrag erhöht.</p><p>Daneben werden gemäß <a href=\"https://www.landesrecht-bw.de/jportal/?quelle=jlink&amp;query=BesG+BW+%C2%A7+88&amp;psml=bsbawueprod.psml&amp;max=true\" target=\"_blank\" rel=\"noopener noreferrer\">§ 88 S. 3</a> iVm <a href=\"https://www.landesrecht-bw.de/jportal/?quelle=jlink&amp;query=BesG+BW+%C2%A7+79&amp;psml=bsbawueprod.psml&amp;max=true\" target=\"_blank\" rel=\"noopener noreferrer\">§ 79 Abs. 2 S. 2 und 3</a> des Landesbesoldungsgesetzes Baden-Württemberg (LBesGBW) ein <strong>Familienzuschlag</strong> - nicht aber weitergehende Leistungen - gewährt. Bei unentschuldigtem Fernbleiben vom Dienst wird gemäß <a href=\"https://www.landesrecht-bw.de/jportal/?quelle=jlink&amp;query=BesG+BW+%C2%A7+11&amp;psml=bsbawueprod.psml&amp;max=true\" target=\"_blank\" rel=\"noopener noreferrer\">§ 11 LBesGBW</a> der Verlust der Unterhaltsbeihilfe festgestellt. Weitere Informationen zur Unterhaltsbeihilfe und zum Familienzuschlag findest du auf der <a href=\"https://lbv.landbw.de/service/fachliche-themen/auszubildende/rechtsreferendare\" target=\"_blank\" title=\"Landesamt für Besoldung &amp; Versorgung\" rel=\"noopener noreferrer\">Webseite des Landesamtes für Besoldung und Versorgung</a> (LBV).</p><p>Erhälst du von dritter Seite ein Entgelt, etwa im Rahmen der Ausbildung oder aus einer anderen Nebentätigkeit, wird es auf die Unterhaltsbeihilfe angerechnet, soweit es 150% der Unterhaltsbeihilfe - zurzeit also einen Betrag von 2.028,77 Euro - übersteigt; gegebenenfalls lohnt sich unter lohnsteuerrechtlichen Gesichtspunkten die Angabe der Nebentätigkeit als Haupttätigkeit.</p><p>Eventuell hast du einen Anspruch auf Wohngeld. Darüber kannst du dich beim Wohnungs- bzw. Liegenschaftsamt deiner Stadt informieren. Es zählt der Monat der Antragstellung, soweit die Voraussetzungen ansonsten vorliegen.</p>"
        },
        "id": "ac3c8489-2f56-4161-9ea9-05596d01ab0b",
        "isHidden": false,
        "type": "text"
    },
    {
        "content": {
            "level": "h3",
            "text": "Steuern &amp; Versicherung"
        },
        "id": "3f5be84d-e1f3-4b41-9354-b8cc7f557a23",
        "isHidden": false,
        "type": "heading"
    },
    {
        "content": {
            "text": "<p>Während der Dauer des Vorbereitungsdienstes bist du versicherungspflichtig, es besteht <strong>Beitragspflicht zur Kranken-, Pflege- und Arbeitslosenversicherung</strong>. Von der Unterhaltsbeihilfe werden diese daher neben <strong>Lohnsteuer</strong> und gegebenenfalls Kirchensteuer einbehalten und direkt abgeführt. Nach Abzug von Steuern und Versicherung verbleiben dir ca. 1.150,00 Euro, bei Familienzuschlägen entsprechend mehr.</p><p>Nach <a href=\"https://www.landesrecht-bw.de/jportal/?quelle=jlink&amp;query=JAG+BW+%C2%A7+7&amp;psml=bsbawueprod.psml&amp;max=true\" target=\"_blank\" rel=\"noopener noreferrer\">§ 7 Abs. 1 S. 2 JAG</a> werden außerdem Anwartschaften auf Versorgung bei verminderter Erwerbsfähigkeit und im Alter nach beamtenrechtlichen Vorschriften gewährleistet.</p><p>Trittst du nach der Zweiten juristischen Staatsprüfung nicht in den öffentlichen Dienst ein, so wirst du für die Dauer des Referendariats in der Rentenversicherung der Angestellten nachversichert.</p>"
        },
        "id": "956e8e44-7fc3-434f-ab40-2dc7470c478e",
        "isHidden": false,
        "type": "text"
    },
    {
        "content": {
            "level": "h3",
            "text": "Urlaub"
        },
        "id": "b302fb8a-e0af-402f-a8c7-e0bd9fcc6f8a",
        "isHidden": false,
        "type": "heading"
    },
    {
        "content": {
            "text": "<p>Während des Referendariats hast du pro Ausbildungsjahr (Oktober - September bzw. April - März) <strong>30 Urlaubstage</strong>. Das Antragsformular erhältst du nur bei deinem LG, es lohnt sich, stets einen Antrag innerhalb der AG zu haben, der bei Bedarf kopiert werden kann. Der Urlaub aus dem <strong>ersten</strong> Ausbildungsjahr kann bis zu <strong>neun Monate</strong> über das Dienstjahr hinaus in Anspruch genommen werden.</p>"
        },
        "id": "03837c0c-2140-4b60-945c-0182435b8d91",
        "isHidden": false,
        "type": "text"
    },
    {
        "content": {
            "level": "h3",
            "text": "Zusatzqualifikationen"
        },
        "id": "1e1c38d2-273e-449e-a39d-3b888ef5f821",
        "isHidden": false,
        "type": "heading"
    },
    {
        "content": {
            "text": "<p>Die OLGs organisieren außerdem eine <strong>Vielzahl von Zusatzqualifikationen</strong>, an denen du freiwillig und gegen einen <strong>geringen Selbstkostenanteil</strong> teilnehmen kannst; hierüber wirst du im Vorfeld einer Veranstaltung informiert und erhälst einen <strong>Anmeldebogen</strong>. Bitte beachte die Anmeldefristen, die ebenfalls dort ausgewiesen sind.</p><p>Die Themen der Fortbildungsveranstaltungen sind breit gefächert und umfassen neben betriebswirtschaftlichen Topoi wie dem \"Lesen und Verstehen von Bilanzen\" gerade auch den in der Praxis bedeutsamen Umgang mit Sprache und Rhetorik im juristischen Alltag, etwa mit Angeboten zur \"Kommunikation\", \"Sprechtraining\" sowie \"Legal English\"; in jüngerer Zeit treten zunehmend auch technologische Fragestellungen hinzu, etwa \"KI im Recht\" oder auch \"Legal Tech\".</p>"
        },
        "id": "3176f155-7101-47de-9375-a97b2169c686",
        "isHidden": false,
        "type": "text"
    },
    {
        "content": {
            "level": "h2",
            "text": "Arbeitsgemeinschaften"
        },
        "id": "756fefdf-e57a-4ca5-88e6-a5d420388a27",
        "isHidden": false,
        "type": "heading"
    },
    {
        "content": {
            "text": "<p>Alle Referendar:innen, die zu einem bestimmten Termin in einem Landgerichtsbezirk anfangen, werden in Arbeitsgemeinschaften (\"AGs\") zusammengefasst. Innerhalb der ersten Wochen des Referendariats wählt jede AG eine:n <strong>Kurssprecher:in und eine:n Stellvertreter:in</strong>. Diese vertreten die AG gegenüber der Ausbildungsleitung der jeweiligen Landgerichte und kümmern sich darum, die Wünsche und Forderungen der AG hinsichtlich Umsetzung von Lerninhalten und Dozenten durchzusetzen.</p><p>Jede:r Kurssprecher:in ist während seiner Amtszeit <strong>Mitglied der Sprecherkonferenz</strong> und zur Teilnahme an der Konferenz verpflichtet. Die Sprecherkonferenz tagt in der Regel einmal im Jahr in Stuttgart - in Zeiten von <a href=\"/referendariat/corona\" rel=\"noopener noreferrer\">Corona</a> jedoch online.</p>"
        },
        "id": "1d6ab261-cbe1-4556-a7a6-6eac55dff9cd",
        "isHidden": false,
        "type": "text"
    },
    {
        "content": {
            "text": "<p><strong>Praxistipp:</strong> Um die Erreichbarkeit der \"neuen\" AGs zu erleichtern, sollten die gewählten Kurssprecher:innen dem Sprechervorstand ihre Mailadressen zukommen lassen!</p>"
        },
        "id": "96dce6d9-3526-45a5-a4a4-d7ad62f70914",
        "isHidden": false,
        "type": "note"
    },
    {
        "content": {
            "text": "<p>Die AG trifft sich in regelmäßigen Abständen zu <strong>Lehrveranstaltungen</strong>, die je nach Standort am Landgericht oder in einem entsprechenden Ausbildungsgebäude erfolgen. Einen wesentlichen Bestandteil dieser Lehrveranstaltungen, insbesondere der späteren, stellen Klausurbesprechungen dar. Hierbei werden zumeist originale Examensaufgaben einige Tage vorher ausgegeben und dann in der Veranstaltung besprochen.</p><p>Außerdem werden Übungsklausuren und Probeexamina angeboten. Derzeit ist es <strong>Pflicht, mindestens vier Übungsklausuren und alle acht Probeexamensklausuren mitzuschreiben</strong>. Wir raten dir, erstere so früh wie möglich mitzuschreiben, da Dir gerade zu Beginn des Referendariats ausreichend Zeit dafür zur Verfügung steht. Spätestens in der Anwaltsstation I, teilweise bereits während der Strafstation, wird dir sichtlich die Zeit davonlaufen. Neben den Stationsarbeiten nimmt auch die Menge an Lerninhalten über die Stationen deutlich zu.</p>"
        },
        "id": "a8c1bf9d-e2a2-421b-9a38-8c3012289689",
        "isHidden": false,
        "type": "text"
    },
    {
        "content": {
            "level": "h3",
            "text": "Ausbildungspersonalräte"
        },
        "id": "41d59c0c-08e4-47ed-b8c2-01961caf1688",
        "isHidden": false,
        "type": "heading"
    },
    {
        "content": {
            "text": "<p>Daneben gibt es an manchen Landgerichten traditionell einen <strong>Ausbildungspersonalrat</strong> (\"APR\"), der ebenfalls die Interessen der AG-Mitglieder gegenüber der Ausbildungsleitung vertritt. Der APR hat insbesondere bei disziplinarrechtlichen Maßnahmen, wie bei Verfall oder Kürzungen von Unterhaltsbeihilfen, Mitspracherechte, über die die AG-Sprecher nicht verfügen.</p>"
        },
        "id": "9b620eae-a9cf-4c74-8870-4ed946d69bab",
        "isHidden": false,
        "type": "text"
    },
    {
        "content": {
            "level": "h2",
            "text": "Sonstiges"
        },
        "id": "92a407a3-eac2-4654-bee0-1665ab000b45",
        "isHidden": false,
        "type": "heading"
    },
    {
        "content": {
            "level": "h3",
            "text": "Zugriff auf Online-Datenbanken"
        },
        "id": "91c5d7e1-e4c0-4a26-a4c6-73359690a6b9",
        "isHidden": false,
        "type": "heading"
    },
    {
        "content": {
            "text": "<p>Du erhältst für die gesamte Dauer deines Referendariats einen kostenlosen <strong>Zugang zu juris</strong>. Die Zugangsdaten übersendet die juris GmbH unmittelbar an dich. Dasselbe gilt seit Anfang des Jahres 2021 auch für <strong>Beck-Online</strong>.</p>"
        },
        "id": "8ecdae6d-7bae-480a-9618-4ac69799f682",
        "isHidden": false,
        "type": "text"
    },
    {
        "content": {
            "level": "h3",
            "text": "Referendarausweis"
        },
        "id": "1ea526db-6aaa-491b-abbc-9837ee2e9f8e",
        "isHidden": false,
        "type": "heading"
    },
    {
        "content": {
            "text": "<p>Während der Referendarszeit bekommst du einen <strong>Referendarausweis</strong>. Damit kannst du \"zu Ausbildungszwecken\" an nicht-öffentlichen Verhandlungen teilnehmen und dir einen <a href=\"https://www.isic.de\" target=\"_blank\" rel=\"noopener noreferrer\">ISIC</a> (International Student Identity Card) - Studentenausweis anfertigen lassen, beispielsweise im Reisebüro. Die ISIC wird häufig als normaler Studentenausweis anerkannt. Auf diesem Wege kannst du auch weiterhin zahlreiche Studentenvergünstigungen nutzen. Hierfür benötigst du lediglich ein Passbild.</p>"
        },
        "id": "51593054-32c8-4fb3-8ee1-b02640552057",
        "isHidden": false,
        "type": "text"
    }
]

----

Heading: 

----

Text: Für deinen erfolgreichen Start ins Referendariat findest du hier viele Informationen und erhälst einen Einblick, welche Herausforderungen auf dich zukommen.

----

References: 

----

Downloads: 

----

Uuid: 54YChZcKjx2yqwGb
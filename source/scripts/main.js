'use strict';

/*
* Imports
*/

import jsDetect from './partials/jsDetect';

import Alpine from 'alpinejs';
import Loadeer from 'loadeer';
import Macy from 'macy';
import * as basicLightbox from 'basiclightbox';


/*
* App Class
*/

class App {
    static start() {
        return new App();
    }

    constructor() {
        Promise
        .all([
            App.domReady(),
        ])
        .then(this.init.bind(this));
    }

    static domReady() {
        return new Promise((resolve) => {
            document.addEventListener('DOMContentLoaded', resolve);
        });
    }

    static showPage() {
        document.body.classList.add('app:is-ready');
        console.info('🚀 App:ready');
    }

    init() {
        console.info('🚀 App:init');

        // Avoid 'blank page' on JS error
        try {
            // Check whether JavaScript works
            jsDetect();

            // Lazyload images
            const loadeer = new Loadeer();
            loadeer.observe();

            // Create AlpineJS hooks
            // (1) Pre-initialization
            document.addEventListener('alpine:init', () => {
                Alpine.data('menu', () => ({
                    isOpen: false,
                }))

                Alpine.data('masonry', () => ({
                    init() {
                        return {
                            masonry: Macy({
                                container: this.$el.querySelector('.js-masonry'),
                                trueOrder: false,
                                waitForImages: false,
                                columns: 3,
                                margin: 12,
                                breakAt: {
                                    640: {
                                        columns: 2,
                                        margin: 6,
                                    },
                                }
                            }),
                        }
                    }
                }))

                Alpine.data('lightbox', () => ({
                    show() {
                        const image = this.$el.querySelector('.js-lightbox');

                        basicLightbox.create(`
                            <img src="${image.dataset.lightbox}" width="${image.dataset.width}" height="${image.dataset.height}">
                        `).show();
                    }
                }))

                Alpine.data('video', () => ({
                    isPlaying: false,
                    start() {
                        this.isPlaying = true;
                        this.$refs.iframeElement.setAttribute('src', this.$el.dataset.src)
                    },
                    stop() {
                        this.isPlaying = false;
                        this.$refs.iframeElement.setAttribute('src', '')
                    },
                }))
            })

            // (2) Post-initialization
            document.addEventListener('alpine:initialized', () => {});

            // Initialize AlpineJS
            Alpine.start();

        } catch (err) {
            console.error(err);
        }

        App.showPage();
    }
}

App.start();

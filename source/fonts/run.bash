#!/usr/bin/env bash

set -e

root="$(dirname "$(dirname "$(pwd)")")"
dist="$root/public/assets/fonts"

# EOT
if [[ ! -f $(command -v mkeot) ]]; then
    echo "Command 'mkeot' failed, skipping EOT .."
else
    echo "EOT conversion"
    for file in *.otf
    do
        echo "Converting $file to ${file%.otf}.eot"
        mkeot "$file" > "${file%.otf}.eot" && cp "${file%.otf}.eot" "$dist"
    done
fi


# TTF
if [[ ! -f $(command -v eot2ttf) ]]; then
    echo "Command 'eot2ttf' failed, skipping TTF .."
else
    echo "TTF conversion"
    for file in *.eot
    do
        echo "Converting $file to ${file%.eot}.ttf"
        eot2ttf "$file" "$dist/${file%.eot}.ttf"
        rm "$file"
    done
fi


# WOFF
if [[ ! -f $(command -v sfnt2woff-zopfli) ]]; then
    echo "Command 'sfnt2woff-zopfli' failed, skipping WOFF .."
else
    echo "WOFF conversion"
    for file in *.otf
    do
        echo "Converting $file to ${file%.otf}.woff"
        sfnt2woff-zopfli "$file" &>/dev/null && mv "${file%.otf}.woff" "$dist"
    done
fi


# WOFF2
if [[ ! -f $(command -v woff2_compress) ]]; then
    echo "Command 'woff2_compress' failed, skipping WOFF2 .."
    exit 1
else
    echo "WOFF2 conversion"
    for file in *.otf
    do
        echo "Converting $file to ${file%.otf}.woff2"
        woff2_compress "$file" &>/dev/null && mv "${file%.otf}.woff2" "$dist"
    done
fi

exit 0

<?php

include '../vendor/autoload.php';

use Kirby\Cms\App as Kirby;


$kirby = new Kirby([
    'roots' => [
        'index'    => __DIR__,

        # Base
        'base'     => $base = dirname(__DIR__),
        'content'  => $base . '/content',
        'site'     => $base . '/site',

        # Storage
        'storage'  => $storage = $base . '/storage',
        'accounts' => $storage . '/accounts',
        'cache'    => $storage . '/cache',
        'sessions' => $storage . '/sessions',
    ]
]);

echo $kirby->render();

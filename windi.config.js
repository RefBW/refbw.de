const colors = require('windicss/colors')

module.exports = {
    extract: {
        include: [
            'site/layouts/default.php',
            'site/plugins/core/snippets/video.php',
            'site/plugins/core/snippets/jobs/*.php',
            'site/snippets/**/*.php',
            'site/templates/*.php',
        ],
    },
    safelist: [
        // Declared inside 'hooks'
        'font-medium',

        // Mobile menu
        'flex hidden',

        // Table of contents
        'rounded-l rounded-tl',

        // Icon sizes
        'w-4 h-4',
        'w-5 h-5',
        'w-6 h-6',
        'w-7 h-7',
        'w-8 h-8',
        'w-12 h-12',

        // Component symbols
        'w-10 h-10 mr-4',
        'w-16 h-16 mb-4',
        'w-24 h-24 mb-4',

        // Sidebar links
        'p-2',  // sm
        'p-3',  // lg

        // Gaps (icons, cards, ..)
        'ml-2 mr-2',
        'lg:ml-2 lg:mr-2',
    ],
    darkMode: false,
    theme: {
        container: {
            center: true,
        },
        screens: {
            xs: '480px',
            sm: '640px',
            md: '768px',
            lg: '1024px',
            xl: '1280px',
        },
        colors: {
            transparent: 'transparent',
            current: 'currentColor',
            primary: colors.emerald,
            secondary: colors.amber,

            black: colors.black,
            white: colors.white,
            gray: colors.coolGray,
            red: colors.red,
        },
        fontFamily: {
            serif: [
                'Bitter',
                'ui-serif',
                'Georgia',
                'Cambria',
                '"Times New Roman"',
                'Times',
                'serif'
            ],
        },
    },
    shortcuts: {},
    variants: {
        extend: {},
    },
};

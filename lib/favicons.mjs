import fs from 'fs';
import favicons from 'favicons';

fs.readFile('./package.json','utf-8', (error, json) => {
    if (error) {
        console.log(error.message);
        return;
    }

    const pkg = JSON.parse(json);

    favicons('source/refbw-square_optimized.svg',
        // Configuration
        {
            path: '/favicons/',
            appName: pkg.name,
            appDescription: pkg.description,
            developerName: pkg.author.name,
            developerURL: pkg.author.homepage,
            lang: 'de-DE',
            theme_color: '#059669',
            start_url: '/',
            version: pkg.version,
            icons: {
                android: true,
                appleIcon: true,
                appleStartup: false,
                favicons: true,
                windows: true,
                yandex: false,
            },
            output: {
                images: true,
                files: true,
                html: true,
            },

        // Callback
        }, (error, response) => {
            // If error occurs ..
            if (error) {
                // .. report it
                console.error(error.message);
            }

            // .. otherwise ..
            else {
                // .. write data to disk
                const path = './public/favicons';

                // (1) Favicons
                response.images.map(image => {
                    fs.writeFileSync(`${path}/${image.name}`, image.contents)
                })

                // (2) Data files
                response.files.map(file => {
                    fs.writeFileSync(`${path}/${file.name}`, file.contents)
                })

                // (3) HTML file
                const stream = fs.createWriteStream('./site/snippets/layout/head/favicons.php');

                // (a) Write data
                response.html.forEach(value => stream.write(`${value}\n`));

                // (b) Handle errors
                stream.on('error', (error) => {
                    console.error(error);
                });

                // (c) Close stream
                stream.end();
            }
        }
    );
});

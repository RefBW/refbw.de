# Custom profanity dictionary for refbw.de
#
# For more information,
# see https://github.com/ConsoleTVs/Profanity

import glob
import json

from os.path import abspath, basename, dirname, join


# Determine base directory
base_dir = dirname(__file__)

# Define output file
json_file = join(dirname(base_dir), 'site/config/extras/profanities.json')

# Define blocked words
block_list = [
    'nicht',  # NL = 'faggot' but DE = 'not'
    'am',     # TR = 'cunt' but DE = 'on (day)'
]

# Create data array
words = []

# Iterate over dictionary files ..
for dict_file in sorted(glob.glob(join(base_dir, 'profanities/*'))):
    # .. open each of them ..
    with open(dict_file, 'r', encoding = 'utf8') as file:
        # .. loop over their lines ..
        for line in file:
            # .. convert them to words ..
            word = line.rstrip()

            # .. and unless they are blocklisted ..
            if word in block_list:
                continue

            # .. add them as profanity
            words.append({
                'language': basename(dict_file),
                'word': word,
            })

with open(json_file, 'w') as file:
    json.dump(words, file, indent = 4)
